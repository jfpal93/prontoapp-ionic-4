import { Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { AccordionListGroupComponent } from './accordionlist-group.component';

@Component({
  selector: 'accordionlist',
  template: `
    <ng-content></ng-content>
`,
  styleUrls: ['./accordionlist.component.css']
})
export class AccordionListComponent  implements AfterContentInit {
  @ContentChildren(AccordionListGroupComponent) 
  groups: QueryList<AccordionListGroupComponent>;

  /**
   * Invoked when all children (groups) are ready
   */
  ngAfterContentInit() {
    // Set active to first element
    this.groups.toArray()[0].opened = false;
    // Loop through all Groups
    this.groups.toArray().forEach((t) => {
      // when title bar is clicked
      // (toggle is an @output event of Group)
      t.toggle.subscribe(() => {
        // Open the group
        this.openGroup(t);
      });
      /*t.toggle.subscribe((group) => {
        // Open the group
        this.openGroup(group);
      });*/
    });
  }

  /**
   * Open an accordion group
   * @param group   Group instance
   */
  openGroup(group:any) {
    // close other groups

    if(this.groups.toArray()[0].opened){
      this.groups.toArray().forEach((t) => t.opened = true);
      // open current group
      group.opened = false;
    }
    else{
      this.groups.toArray().forEach((t) => t.opened = false);
      // open current group
      group.opened = true;
    }
    
    
  }
}
