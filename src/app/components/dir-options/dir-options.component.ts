import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Events, NavParams, PopoverController } from '@ionic/angular';
import { Direccion } from 'src/app/models/direccion';
import { DireccionServiceService } from 'src/app/services/direccion-service.service';
import { Storage } from '@ionic/storage';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-dir-options',
  templateUrl: './dir-options.component.html',
  styleUrls: ['./dir-options.component.scss'],
})
export class DirOptionsComponent implements OnInit {

  private dir:Direccion= new Direccion;

  constructor(
    private events: Events,
    private navParams: NavParams,
    private popoverController: PopoverController,
    private router:Router,
    private dirService:DireccionServiceService,
    private storage:Storage
  ) {
    this.dir = this.navParams.get('direccion');
   }

  ngOnInit() {}

  editDireccion()
  {
    let navigationExtras: NavigationExtras = {
      state: {
        edit: true,
        direccion:this.dir
      }
    };
    this.router.navigate(['/add-direccion'], navigationExtras).then(()=>{
      this.popoverController.dismiss();
    });
    
  }
  delDireccion()
  {
    this.storage.get('user').then((data:User)=>{
      if(data){
        this.dirService.deleteDireccion(this.dir,data);
        this.popoverController.dismiss();
      }
    });
    
  }

}
