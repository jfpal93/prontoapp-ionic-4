import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { ConfigureService } from './configure.service';
import { Observable, forkJoin, of, throwError } from 'rxjs';
import { CartServiceService } from './cart-service.service';
import { User } from '../models/user';
import { PedidosService } from './pedidos.service';


@Injectable()
export class ApiServiceService {


  constructor(
    private configure:ConfigureService,
    public toastCtrl: ToastController,
    private http:HttpClient,
    private cartService:CartServiceService,
    private ordenService:PedidosService
  ) 
  {
   }

   getData(user:User): Observable<any[]> {
    let promises = new Array();
    promises.push(this.getResource('getAllData'));
    // let allData = this.getResource('getAllData');
    // let cart;
    // let ordenes;
    // alert(JSON.stringify(user));

    // let promises = new Array();
    // promises.push(function1());
    // promises.push(function2());

   

    if(user){
      promises.push(this.cartService.getCart(user));
      promises.push(this.ordenService.getPedidosDR(user));
      // cart= this.cartService.getCart(user);
      // ordenes= this.ordenService.getPedidosDR(user);
    }
    else{
      promises.push(this.cartService.createCart());
      // cart=this.cartService.createCart();
    }

    // return forkJoin([allData,cart,ordenes]);
    return  forkJoin(promises);
  }

  

  getResource(route:string){
        
    return this.http.get(this.configure.getUrl()+route,
    {
      headers: new HttpHeaders({
        'Accept': 'application/json;charset=utf-8',
        'Content-Type': 'application/json;charset=utf-8'
      })
    });
  }

  checkContent(route:any){
    // return new Promise( (resolve, reject) => {

    //     this.storage.get(route).then((val) => {
    //         if(val===null){
    //             resolve("Empty!");
    //         }
    //         else{

    //             // this.storage.remove(route);
    //             resolve("Exito!");
    
    //         }
    //     });
    // });

}
}
