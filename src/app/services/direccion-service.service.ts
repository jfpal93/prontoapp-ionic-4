import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { LoadingController, Events, ToastController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigureService } from './configure.service';
import { Storage } from '@ionic/storage';
import { Direccion } from '../models/direccion';

@Injectable()
export class DireccionServiceService {

  constructor(
    public loadingCtrl:LoadingController,

    private http:HttpClient,

    private configure:ConfigureService,
    private storage:Storage,
    public event:Events,

    public toastCtrl:ToastController

  ) { }

  async addDireccion(dir:any,user:User){
    const loading=await this.loadingCtrl.create({
      message:'Guardando nueva dirección...'
    });
    await loading.present();
    return new Promise((resolve, reject) => {
        this.http.post(
            this.configure.getUrl()+"direccion/"+user.id+"/add",
        {
            "nombre":dir.nombre,
            "direccion":dir.direccion,
            "lat":dir.lat,
            "lng":dir.lng,
            
        },
        
        {
            headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
            })
        })
        .subscribe((res:any) => {
                
            let dirs:Direccion[]=[];

            for(var d of res){
                let dir = new Direccion;
                dir.id=d.id;
                dir.direccion=d.direccion;
                dir.lat=d.lat;
                dir.lng=d.lng;
                dir.nombre=d.nombre;
                dir.user_id=d.user_id;
                if(d.selected ==0){
                    dir.selected=false;
                }
                if(d.selected ==1){
                    dir.selected=true;
                }
                dirs.push(dir);
            }
            
            this.storage.set("direcciones",dirs).then(()=>{
                this.event.publish('user:direcciones',true);
                this.event.publish('user:direccion-created',true);
                this.showToast("Dirección creada...");
            });
            loading.dismiss();
        }, (err) => {
            loading.dismiss();
            reject(err);
            this.showToast(err.error);
        });

        

    });

}

async editDireccion(dir:any,user:User,idDir:any){
    const loading=await this.loadingCtrl.create({
      message:'Modificando dirección...'
    });
    await loading.present();
    return new Promise((resolve, reject) => {
        this.http.post(
            this.configure.getUrl()+"direccion/"+user.id+"/edit",
        {
            "id":idDir,
            "nombre":dir.nombre,
            "direccion":dir.direccion,
            "lat":dir.lat,
            "lng":dir.lng,
            
        },
        
        {
            headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
            })
        })
        .subscribe((res:any) => {
                
            let dirs:Direccion[]=[];

            for(var d of res){
                let dir = new Direccion;
                dir.id=d.id;
                dir.direccion=d.direccion;
                dir.lat=d.lat;
                dir.lng=d.lng;
                dir.nombre=d.nombre;
                dir.user_id=d.user_id;
                if(d.selected ==0){
                    dir.selected=false;
                }
                if(d.selected ==1){
                    dir.selected=true;
                }
                dirs.push(dir);
            }
            
            this.storage.set("direcciones",dirs).then(()=>{
                this.event.publish('user:direcciones',true);
                this.event.publish('user:direccion-edited',true);

            });
            loading.dismiss();
        }, (err) => {
            loading.dismiss();
            reject(err);
            this.showToast(err.error);
        });

        

    });

}

async getDirecciones(user:User){
    const loading=await this.loadingCtrl.create({
      message:'Obteniendo direcciones...'
    });
    await loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(
                this.configure.getUrl()+"direccion/"+user.id,
            
            {
              headers: new HttpHeaders({
                'Accept': 'application/json;charset=utf-8',
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+user.access_token
              })
             }
            
            ).subscribe((res:any) => {
                
                let dirs:Direccion[]=[];

                for(var d of res){
                    let dir = new Direccion;
                    dir.id=d.id;
                    dir.direccion=d.direccion;
                    dir.lat=d.lat;
                    dir.lng=d.lng;
                    dir.nombre=d.nombre;
                    dir.user_id=d.user_id;
                    if(d.selected ==0){
                        dir.selected=false;
                    }
                    if(d.selected ==1){
                        dir.selected=true;
                    }
                    dirs.push(dir);
                }
                
                this.storage.set("direcciones",dirs).then(()=>{
                    this.event.publish('user:direcciones',true);
                });
                
                loading.dismiss();
                
            }, (err) => {
                reject(err);
                loading.dismiss();
                this.showToast(err.error);
            });

            

        });
    
        
    // });


}


async selectDireccion(user:User,id:any){
    const loading=await this.loadingCtrl.create({
        message:'Guardando opción...'
    });
    await loading.present();
        return new Promise((resolve, reject) => {
            this.http.post(
                this.configure.getUrl()+"direccion/"+user.id+"/select",
            {
                "dirId":id
            },
            
            {
              headers: new HttpHeaders({
                'Accept': 'application/json;charset=utf-8',
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+user.access_token
              })
             }
            
            ).subscribe((res:any) => {
                let dirs:Direccion[]=[];

                for(var d of res){
                    let dir = new Direccion;
                    dir.id=d.id;
                    dir.direccion=d.direccion;
                    dir.lat=d.lat;
                    dir.lng=d.lng;
                    dir.nombre=d.nombre;
                    dir.user_id=d.user_id;
                    if(d.selected ==0){
                        dir.selected=false;
                    }
                    if(d.selected ==1){
                        dir.selected=true;
                    }
                    dirs.push(dir);
                }
                
                this.storage.set("direcciones",dirs).then(()=>{
                    this.event.publish('user:direcciones',true);
                });
                loading.dismiss();
                
            }, (err) => {
                reject(err);
                // alert(JSON.stringify(err));
                loading.dismiss();
                this.showToast(err.error);
            });

            

        });


    }

    async deselectDireccion(user:User,id:any){
        const loading=await this.loadingCtrl.create({
            message:'Guardando opción...'
        });
        await loading.present();
        return new Promise((resolve, reject) => {
            this.http.post(
                this.configure.getUrl()+"direccion/"+user.id+"/deselect",
            {
                "dirId":id
            },
            
            {
                headers: new HttpHeaders({
                'Accept': 'application/json;charset=utf-8',
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+user.access_token
                })
                }
            
            ).subscribe((res:any) => {
                let dirs:Direccion[]=[];

                for(var d of res){
                    let dir = new Direccion;
                    dir.id=d.id;
                    dir.direccion=d.direccion;
                    dir.lat=d.lat;
                    dir.lng=d.lng;
                    dir.nombre=d.nombre;
                    dir.user_id=d.user_id;
                    if(d.selected ==0){
                        dir.selected=false;
                    }
                    if(d.selected ==1){
                        dir.selected=true;
                    }
                    dirs.push(dir);
                }
                // dir.id=res

                
                
                this.storage.set("direcciones",dirs).then(()=>{
                    this.event.publish('user:direcciones',true);
                });
                loading.dismiss();
                
            }, (err) => {
                reject(err);
                // alert(JSON.stringify(err));
                loading.dismiss();
                this.showToast(err.error);
            });

            

        });
    
        
    // });


    }

    async deleteDireccion(direcc:any,user:User){
        const loading=await this.loadingCtrl.create({
          message:'Eliminando dirección...'
        });
        await loading.present();
        return new Promise((resolve, reject) => {
            this.http.post(
                this.configure.getUrl()+"direccion/"+user.id+"/delete",
            {
                "id":direcc.id
            },
            
            {
                headers: new HttpHeaders({
                'Accept': 'application/json;charset=utf-8',
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+user.access_token
                })
            })
            .subscribe((res:any) => {
                    
                let dirs:Direccion[]=[];
    
                for(var d of res){
                    let dir = new Direccion;
                    dir.id=d.id;
                    dir.direccion=d.direccion;
                    dir.lat=d.lat;
                    dir.lng=d.lng;
                    dir.nombre=d.nombre;
                    dir.user_id=d.user_id;
                    if(d.selected ==0){
                        dir.selected=false;
                    }
                    if(d.selected ==1){
                        dir.selected=true;
                    }
                    dirs.push(dir);
                }
                
                this.storage.set("direcciones",dirs).then(()=>{
                    this.event.publish('user:direcciones',true);
                    this.showToast("Dirección eliminada...");
    
                });
                loading.dismiss();
            }, (err) => {
                loading.dismiss();
                reject(err);
                this.showToast(err.error);
            });
    
            
    
        });
    
    }

    async showToast(msj:string){
        
        const DeclinedToast =await  this.toastCtrl.create({
            message: msj,
            duration: 4000,
            position: 'bottom',
            buttons: [ {
                text: 'Cerrar',
                role: 'cancel'
              }
            ]
        });
        DeclinedToast.present();
      }
}
