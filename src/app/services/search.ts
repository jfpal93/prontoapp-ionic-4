import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { LoadingController, Events, ToastController } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ConfigureService } from './configure.service';
import { Storage } from '@ionic/storage';
import { Observable, forkJoin, of, throwError } from 'rxjs';
import { Establecimiento } from '../models/establecimiento';
import { Producto } from '../models/producto';
import { Categoria } from '../models/categoria';


@Injectable()
export class SearchService {


  // products:Producto[]=[];
  // categorias:Categoria[]=[];
  // establecimientos:Establecimiento[]=[];
  storageCat:Categoria[] =[]; 

  constructor(
    public loadingCtrl:LoadingController,

    private http:HttpClient,

    private configure:ConfigureService,
    private storage:Storage,
    public event:Events,

    public toastCtrl:ToastController

  ) { }



  async search(text:string){
    
    let storageCat=await this.storage.get('categorias');

    let catFiltered=storageCat.filter((cat:Categoria)=>{
      return cat.nombre.toLowerCase().indexOf(text)> -1;
    });

    let storageEstab=await this.storage.get('establecimientos');

    let EstabFiltered=storageEstab.filter((estab:Establecimiento)=>{
      return estab.nombre.toLowerCase().indexOf(text)> -1;
    });

    let storageProds=await this.storage.get('productos');

    let prodsFiltered=storageProds.filter((prod:Producto)=>{
      return prod.nombre.toLowerCase().indexOf(text)> -1 || prod.descripcion.toLowerCase().indexOf(text)>-1;
    });

    var compEstabCat=storageEstab.filter(o => storageCat.some(({id,nombre}) => o.categoria_id === id && nombre.toLowerCase().indexOf(text)> -1 ));
    var compEstabProd=storageProds.filter(o => storageEstab.some(({id,nombre}) => o.establecimiento_id === id && nombre.toLowerCase().indexOf(text)> -1 ));

    var compEstabCatProd=storageProds.filter(o=>compEstabCat.some(({id})=>o.establecimiento_id === id));

    // return Observable.from()
    
    return forkJoin([compEstabCat,compEstabProd,compEstabCatProd,catFiltered,EstabFiltered,prodsFiltered]);
    

  }

  saveSearch(text:string,id:any){

    if(id){
      return this.http.post(
        this.configure.getUrl()+"search/save",
        {
          "search":text+"",
          "user_id":id+""
        },
    
      {
          headers: new HttpHeaders({
              'Accept': 'application/json;charset=utf-8',
              'Content-Type': 'application/json;charset=utf-8'
            })
      });
    }
    else{
      return this.http.post(
        this.configure.getUrl()+"search/save",
        {
          "search":text+""
        },
    
      {
          headers: new HttpHeaders({
              'Accept': 'application/json;charset=utf-8',
              'Content-Type': 'application/json;charset=utf-8'
            })
      });
    }
    
  }


  getTop10(){
    let params = new HttpParams();

    // Begin assigning parameters
    // params = params.append('search', text);
    return this.http.get(
        this.configure.getUrl()+"search/top10",
    
    {
        headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8'
          })
     }
    
    )
  }

  getTop10ByUser(user:User){
    let params = new HttpParams();

    // Begin assigning parameters
    params = params.append('user_id', user.id+"");
    return this.http.get(
        this.configure.getUrl()+"search/getTop10ByUser",
    
      {
        headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
          }),
          params: params
      }
    
    )
  }


}