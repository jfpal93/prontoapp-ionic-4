import { Injectable } from '@angular/core';
import { FCM } from '@ionic-native/fcm/ngx';

@Injectable({
  providedIn: 'root'
})
export class FCMService {

  constructor(private fcm: FCM) { }

  getToken(){
    return this.fcm.getToken();
  }

  getNotification(){
    return this.fcm.onNotification();
  }

  checkPermissions(){
    return this.fcm.hasPermission();
  }
}
