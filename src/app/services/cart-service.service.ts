import { Injectable } from '@angular/core';
import { ProductsCarted } from '../models/productsCarted';
import { Producto } from '../models/producto';
import { User } from '../models/user';
import { Cart } from '../models/cart';
import { Storage } from '@ionic/storage';
import { Events, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { ConfigureService } from './configure.service';
import { Observable, forkJoin, of, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Orden } from '../models/orden';
import { Pedido } from '../models/pedido';

@Injectable()
export class CartServiceService {

  constructor(
    private storage:Storage,
    private events:Events,
    private http:HttpClient,
    private configure:ConfigureService,
    public toastCtrl:ToastController,
    public loadingCtrl:LoadingController,
    private alertController:AlertController
  ) { }

  getCart(user:User){
        
      return this.http.get(this.configure.getUrl()+"carrito/"+user.id
      ,
        
        {
          headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
          })
        });
  }

    getCartOnly(user:User): Observable<any[]>{
        let cart;
        if(user){
            cart= this.getCart(user);
            // cart=this.checkProductsInCart(user);
        }
        else{
            cart=this.createCart();
        }
  
      return forkJoin([cart]);
    
    }

    async checkProductsInCart(user:User){
      const loading=await this.loadingCtrl.create();
      await loading.present();

      let cart:Cart = await this.storage.get('cart');

      if(cart.quantity>0){
        let productsCarted:ProductsCarted[]=cart.products;
        for(let value of productsCarted){          
          
          this.addProductsBackground(value.product,value.count,user);
        }
        loading.dismiss();
        return this.getCart(user);
        
      }
      else{
        loading.dismiss();
        return this.getCart(user);
      }
    }

  getUserCartSaved(res){
      
    let cart=new Cart();
    // cart.iva=0.0;
    cart.id=res[0].carrito_id;
    
    let prods:ProductsCarted[]=[];
    for(var prod of res[0].products){
      let p = new Producto;
      p.id = prod.id;
      p.linkImagen=prod.linkImagen;
      p.nombre=prod.nombre;
      p.precio=prod.precio;
      p.disponible=prod.disponible;
      p.descripcion=prod.descripcion;
      p.establecimiento_id=prod.establecimiento_id

      let pc = new ProductsCarted;
      pc.product=p;
      pc.count=prod.cant;

      prods.push(pc);

    }
    cart.products=prods;
    cart.quantity=res[0].cant;
    cart.state="idle";
    cart.subtotal=res[0].total;
    cart.total=res[0].total;
    this.storage.set('cart', cart).then(()=>{
        this.events.publish("cart:saved",true);
    }); 
  }

  getUserCartSaved2(res){
    
    let cart=new Cart();
    // cart.iva=0.0;
    cart.id=res[0].carrito_id;
    
    let prods:ProductsCarted[]=[];
    for(var prod of res[0].products){
      let p = new Producto;
      p.id = prod.id;
      p.linkImagen=prod.linkImagen;
      p.nombre=prod.nombre;
      p.precio=prod.precio;
      p.disponible=prod.disponible;
      p.descripcion=prod.descripcion;
      p.establecimiento_id=prod.establecimiento_id

      let pc = new ProductsCarted;
      pc.product=p;
      pc.count=prod.cant;

      prods.push(pc);

    }
    cart.products=prods;
    cart.quantity=res[0].cant;
    cart.state="idle";
    cart.subtotal=res[0].total;
    cart.total=res[0].total;
    return this.storage.set('cart2', cart);
  }

  getUserCartSaved3(){
    return this.storage.get('cart');
  }

  //Funcion que verifica al inicio si existen elementos en carrito si no se ha autenticado
  async createCart(){

    

    let cart:Cart=await this.storage.get('cart');
    if(cart){
      if(cart.quantity>0){
        this.events.publish("cart:saved",true);
        return await this.storage.get('cart');
      }
      else{
        let prods:ProductsCarted[]=[];
        let cart=new Cart();
        // cart.iva=0.0;
        cart.products=prods;
        cart.quantity=0;
        cart.state="idle";
        cart.subtotal=0.0;
        cart.total=0.0;
        return this.storage.set('cart', cart).then(()=>{
            this.events.publish("cart:saved",true);
        }); 
      }
    }
    else{
      let prods:ProductsCarted[]=[];
      let cart=new Cart();
      // cart.iva=0.0;
      cart.products=prods;
      cart.quantity=0;
      cart.state="idle";
      cart.subtotal=0.0;
      cart.total=0.0;
      return this.storage.set('cart', cart).then(()=>{
          this.events.publish("cart:saved",true);
      }); 
    }
    
    // if(cart.quantity>0){

    // }
    // else{

    // }


  }

  eliminateDuplicates(arr) {
    var i,
        len = arr.length,
        out = [],
        obj = {};
  
    for (i = 0; i < len; i++) {
      obj[arr[i]] = 0;
    }
    for (i in obj) {
      out.push(i);
    }
    return out;
  }

  async checkEstabNumberIs2(prod:Producto){
    let estabsid=[];
    let initialLength;

    let cart = await this.storage.get('cart');

    for(var el of cart.products){
      estabsid.push(el.product["establecimiento_id"])
    }
    
    estabsid=this.eliminateDuplicates(estabsid);
    estabsid.push(prod["establecimiento_id"]);
    estabsid=this.eliminateDuplicates(estabsid);
    initialLength=estabsid.length;
    if(initialLength<3){
      return false;
    }
    return true;
  }

  async checkEstabNumberIs22(prod:Producto){
    let estabsid=[];
    let initialLength;

    let cart = await this.storage.get('cart2');

    for(var el of cart.products){
      estabsid.push(el.product["establecimiento_id"])
    }
    
    estabsid=this.eliminateDuplicates(estabsid);
    estabsid.push(prod["establecimiento_id"]);
    estabsid=this.eliminateDuplicates(estabsid);
    initialLength=estabsid.length;
    if(initialLength<3){
      return false;
    }
    return true;
  }

  async addProductToCartBackground(prod:Producto,cant:number,user:User){

    if(!await this.checkEstabNumberIs22(prod)){
        this.storage.get('cart2').then((val:Cart) => {
          let cart=val;
          if(this.ifElementInCart(cart.products,prod)){
              this.addElementInCarted(cart.products,prod,cant);
          }
          else{
              let newProdCart=new ProductsCarted;
              newProdCart.count=cant;
              newProdCart.product=prod;
              cart.products.push(newProdCart);
              
          }
          cart.quantity=cart.quantity+cant;
          cart.subtotal=cart.subtotal+prod.precio;
        //   cart.iva=(cart.subtotal*0.12);
          
          cart.total=cart.subtotal;
          
          this.storage.set('cart2', cart);  
      });

      if(user){

        return new Promise(async (resolve, reject) => {
            this.http.post(
                this.configure.getUrl()+"carrito/"+user.id+"/add",
            {
                "IdProduct":prod.id,
                "cant":cant,
            },
            
            {
              headers: new HttpHeaders({
                'Accept': 'application/json;charset=utf-8',
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+user.access_token
              })
            }
            
            ).subscribe((response) => {
              // loading.dismiss();
              resolve(response);
            }, (err) => {
                reject(err);
                // loading.dismiss();
                this.showToast(err);
            });
        });
      }

    }
      
    else{
      this.presentAlert("Lo sentimos, solo puedes pedir productos de hasta 2 establecimientos a la vez...");
    }
      



  }

  async addProductToCart(prod:Producto,cant:number,user:User){

    if(!await this.checkEstabNumberIs2(prod)){
        this.storage.get('cart').then((val:Cart) => {
          let cart=val;
          if(this.ifElementInCart(cart.products,prod)){
              this.addElementInCarted(cart.products,prod,cant);
          }
          else{
              let newProdCart=new ProductsCarted;
              newProdCart.count=cant;
              newProdCart.product=prod;
              cart.products.push(newProdCart);
              
          }
          cart.quantity=cart.quantity+cant;
          cart.subtotal=cart.subtotal+prod.precio;
        //   cart.iva=(cart.subtotal*0.12);
          
          cart.total=cart.subtotal;
          
          this.storage.set('cart', cart).then(()=>{
            this.events.publish("cartUpdate:addedProduct",true);

          });  
      });

      if(user){

        return new Promise(async (resolve, reject) => {

            const loading=await this.loadingCtrl.create();
            await loading.present();
            this.http.post(
                this.configure.getUrl()+"carrito/"+user.id+"/add",
            {
                "IdProduct":prod.id,
                "cant":cant,
            },
            
            {
              headers: new HttpHeaders({
                'Accept': 'application/json;charset=utf-8',
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+user.access_token
              })
            }
            
            ).subscribe((response) => {
              loading.dismiss();
            }, (err) => {
                reject(err);
                loading.dismiss();
                this.showToast(err);
            });
        });
      }

    }
      
    else{
      this.presentAlert("Lo sentimos, solo puedes pedir productos de hasta 2 establecimientos a la vez...");
    }
      



  }

  addProductsBackground(prod:Producto,cant:number,user:User){
      return new Promise(async (resolve, reject) => {

       
        this.http.post(
            this.configure.getUrl()+"carrito/"+user.id+"/add",
        {
            "IdProduct":prod.id,
            "cant":cant,
        },
        
        {
          headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
          })
        }
        
        ).subscribe((response) => {
          // loading.dismiss();
          resolve(response);
        }, (err) => {
            reject(err);
            // loading.dismiss();
            // esolve(response);
            this.showToast(err);
        });
    });
  }

  deleteProductToCart(prod:Producto,user:User){
      let one:boolean=false;
      this.storage.get('cart').then((val:Cart) => {
          
          let cart:Cart=val;

          if(this.ifElementInCart(cart.products,prod)){
              this.deleteElementInCarted(cart.products,prod,1);
          }
          if(cart.quantity-1>=0){
              cart.quantity=cart.quantity-1;
          }

          let precio =Number(prod.precio);
          let subt=Number(cart.subtotal-precio);
          
          cart.subtotal=subt;
        //   cart.iva=(subt*0.12);
          cart.total=cart.subtotal;


          this.storage.set('cart', cart).then(()=>{
            this.events.publish("cartUpdate:deletedProduct",true);
          }); 


          
      });
      if(user){

        return new Promise(async (resolve, reject) => {
          const loading=await this.loadingCtrl.create();
          await loading.present();
            this.http.post(
                this.configure.getUrl()+"carrito/"+user.id+"/delete",
            {
                "IdProduct":prod.id,
                "cant":1,
            },
            
            {
                headers: new HttpHeaders({
                'Accept': 'application/json;charset=utf-8',
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+user.access_token
                })
            }
            
            ).subscribe((response) => {
                
              loading.dismiss();
            }, (err) => {
                reject(err);
                loading.dismiss();
                this.showToast(err);
            });

            

        });
      }
      


  }

  ifElementInCart(array:ProductsCarted[],element:Producto){


      for(var el of array){
              
          if(el.product["id"]===element["id"]){
              return true;
          }
      }

      return false;

  }

  addElementInCarted(array:ProductsCarted[],element:Producto,cant:number){


      for(var el of array){
              
          if(el.product["id"]===element["id"]){
              el.count=el.count+cant;
          }
      }

  }

  deleteElementInCarted(array:ProductsCarted[],element:Producto,cant:number){

    var vel:boolean=false
    var index;
    for(var el of array){
            
        if(el.product["id"]===element["id"]){
            if(el.count-cant>0){
                el.count=el.count-cant;
            }
            else{
                vel=true;
                index=array.indexOf(el);
            }
        }
    }

    if(vel){
        array.splice(index,1);
        
    }

  }

  async placeOrder(user:User,dirId:any){

    const loading=await this.loadingCtrl.create({
      message:'Registrando Orden...'
    });
    await loading.present();
        
    return new Promise((resolve, reject) => {
        this.http.post(
            this.configure.getUrl()+"carrito/"+user.id+"/placeOrder",
        {
            "dirId":dirId
        },
        
        {
          headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
          })
        }
        
        ).subscribe((response:any) => { 
          this.storage.set('codigo-orden',response).then(()=>{
            this.events.publish("placedOrder",true);
            // this.createCart();
            let prods:ProductsCarted[]=[];
            let cart=new Cart();
            // cart.iva=0.0;
            cart.products=prods;
            cart.quantity=0;
            cart.state="idle";
            cart.subtotal=0.0;
            cart.total=0.0;
            return this.storage.set('cart', cart).then(()=>{
              loading.dismiss();
                this.events.publish("cart:saved",true);
            }); 
            
          })
          
          
          
        }, (err) => {
            reject(err);
            this.showToast(err.error.message);
            
            loading.dismiss();
        });
    });
  }

  async showToast(msj:string){
        
    const DeclinedToast =await  this.toastCtrl.create({
        message: msj,
        duration: 4000,
        position: 'bottom',
        buttons: [ {
            text: 'Cerrar',
            role: 'cancel'
          }
        ]
    });
    DeclinedToast.present();
  }

  async presentAlert(msj:string) {
    const alert = await this.alertController.create({
      header: '¡Alerta!',
      message: msj,
      buttons: ['OK']
    });

    await alert.present();
  }
}
