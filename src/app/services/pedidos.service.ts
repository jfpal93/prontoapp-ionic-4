import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, ToastController, LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigureService } from './configure.service';
import { User } from '../models/user';
import { Orden } from '../models/orden';
import { Pedido } from '../models/pedido';
import { Producto } from '../models/producto';
import { Direccion } from '../models/direccion';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  constructor(
    private storage:Storage,
    private events:Events,
    private http:HttpClient,
    private configure:ConfigureService,
    public toastCtrl:ToastController,
    private loadingCtrl:LoadingController
  ) { }

  getPedidosDR(user:User){
    // return new Promise((resolve, reject) => {
        return this.http.get(
            this.configure.getUrl()+"getOrdenesDR/"+user.id,
        
        {
          headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
          })
        }
        
        )
        // .subscribe((res:any[]) => {
        //   // alert(JSON.stringify(res[0]));
        //   // alert(res.length);
        //   if(res.length>0){
        //     alert(JSON.stringify(res[0]));
            // this.storage.set('orderConfirmationData',res[0]).then(()=>{
            //   this.events.publish('Order:Confirmation',true);
            // });
        //   }

        //   // if(res.pendiente){

        //   // }

        //   // if
          
        //   //   this.storage.set('pedidos', ordenes).then(()=>{
        //   //     this.events.publish("pedidos:saved",true);
        //   //   });
          
          
        // }, (err) => {
        //     reject(err);
        //     this.showToast(err);
        // })
        ;
    // });
  }


  getPedidos(user:User){
        
    return new Promise((resolve, reject) => {
        this.http.get(
            this.configure.getUrl()+"getOrdenes/"+user.id,
        
        {
          headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
          })
        }
        
        ).subscribe((res:any) => {
          let ordenes:Orden[]=[];

          for(var ord of res){
            let orden = new Orden;
            orden.id=ord.orden_id;
            orden.state=ord.estate;
            orden.quantity=ord.cant;
            orden.subtotal=ord.subtotal;
            orden.total=ord.total;
            orden.codigo=ord.orden_codigo;
            orden.cant=ord.cant;
            orden.costo_delivery=ord.costo_delivery;
            orden.tiempo=ord.tiempo;
            orden.tiempoDel=ord.tiempoDel;
            orden.tiempoEstabs=ord.tiempoEstabs;
            orden.nombreCliente=ord.user.name;

            let dir:Direccion=new Direccion;
            dir.direccion=ord.direccion.direccion;
            dir.id=ord.direccion.id;
            dir.lat=parseFloat(ord.direccion.lat+"");
            dir.lng=parseFloat(ord.direccion.lng+"");

            orden.direccion=dir;

            let pedidos:Pedido[]=[];

            for(var ped of ord.pedidos){

              let pedido = new Pedido;

              pedido.establecimiento=ped.establecimiento;
              pedido.estabId=ped.establecimiento_id;
              pedido.id=ped.id;
              pedido.tiempo=ped.tiempo;
              pedido.subtotal=ped.subtotal
              pedido.total=ped.total

              let productos:Producto[]=[];

              for(var prod of ped.productos){
                let producto = new Producto;

                producto.id=prod.id;
                producto.count=prod.cant;
                producto.nombre=prod.nombre;
                producto.linkImagen=prod.linkImagen;
                producto.precio=prod.precio;
                producto.descripcion=prod.descripcion;
                
                productos.push(producto);
              }

              pedido.products=productos;
              pedidos.push(pedido);

            }

            orden.pedidos=pedidos;
            ordenes.push(orden);

          }
          
            this.storage.set('pedidos', ordenes).then(()=>{
              this.events.publish("pedidos:saved",true);
            });
          
          
        }, (err) => {
            reject(err);
            this.showToast(err);
        });
    });
    
  }

  async showToast(msj:string){
        
    const DeclinedToast =await  this.toastCtrl.create({
        message: msj,
        duration: 4000,
        position: 'bottom',
        buttons: [ {
            text: 'Cerrar',
            role: 'cancel'
          }
        ]
    });
    DeclinedToast.present();
  }

  async acceptDelivery(user:User,ordenId:any){
    const loading=await this.loadingCtrl.create({
      message:'Enviando respuesta...'
    });
    await loading.present();
    return new Promise((resolve, reject) => {
        this.http.post(this.configure.getUrl()+"deliveryAccepted",
        {
          "ordenId":ordenId
        },
        {
          headers: new HttpHeaders({
          'Accept': 'application/json;charset=utf-8',
          'Content-Type': 'application/json;charset=utf-8',
          'Authorization': 'Bearer '+user.access_token
          })
        })
        .subscribe((res:any) => {
          this.getPedidos(user);
          loading.dismiss();
        }, (err) => {
          loading.dismiss();
          reject(err);
          this.showToast(err.error);
        });
    });
  }

  async cancelDelivery(user:User,ordenId:any){
    
    const loading=await this.loadingCtrl.create({
      message:'Cancelando pedido...'
    });
    await loading.present();
    return new Promise((resolve, reject) => {
        this.http.post(this.configure.getUrl()+"deliveryCanceled",
        {
          "ordenId":ordenId
        },
        {
          headers: new HttpHeaders({
          'Accept': 'application/json;charset=utf-8',
          'Content-Type': 'application/json;charset=utf-8',
          'Authorization': 'Bearer '+user.access_token
          })
        })
        .subscribe((res:any) => {
          this.getPedidos(user);
          loading.dismiss();
        }, (err) => {
          loading.dismiss();
          reject(err);
          this.showToast(err.error);
        });
    });
  }
  
  async cancelOrder(user:User,ordenId:any){
    const loading=await this.loadingCtrl.create({
      message:'Cancelando pedido...'
    });
    await loading.present();
    return new Promise((resolve, reject) => {
        this.http.post(this.configure.getUrl()+"deliveryCanceled",
        {
          "ordenId":ordenId
        },
        {
          headers: new HttpHeaders({
          'Accept': 'application/json;charset=utf-8',
          'Content-Type': 'application/json;charset=utf-8',
          'Authorization': 'Bearer '+user.access_token
          })
        })
        .subscribe((res:any) => {
          this.events.publish('orden:canceled',true);
          loading.dismiss();
          
        }, (err) => {
          loading.dismiss();
          reject(err);
          this.showToast(err.error);
        });
    });
  }
}
