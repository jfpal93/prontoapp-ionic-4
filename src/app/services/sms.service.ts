import { Injectable } from '@angular/core';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class SmsService {

  constructor(
    private firebase: FirebaseAuthentication,
    private auth:AuthService
    ) { }


  sendOTP(id:string,otp:string,credentials){

    return this.firebase.signInWithVerificationId(id,otp);
    
  }

  sendSMS(phone:string){

    return this.firebase.verifyPhoneNumber("+593"+phone,30000);

  }
}
