import { Injectable } from '@angular/core';
import { LoadingController, Events, ToastController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigureService } from './configure.service';
import { Storage } from '@ionic/storage';
import { User } from '../models/user';
import { CartServiceService } from './cart-service.service';
import { FCMService } from './fcm.service';
import { ApiServiceService } from './api-service.service';
import { Cart } from '../models/cart';
import { ProductsCarted } from '../models/productsCarted';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private firebase: FirebaseAuthentication,
    public loadingCtrl:LoadingController,
    private http:HttpClient,
    private configure:ConfigureService,
    private storage:Storage,
    public event:Events,
    public toastCtrl:ToastController,
    private cartService:CartServiceService,
    private fcmService:FCMService,
    private apiService:ApiServiceService
  ) { }

  async signup(credentials){
    
    const loading=await this.loadingCtrl.create({
      message:'Registrando...'
    });
    await loading.present();

    if(credentials.name === null || credentials.email === null || credentials.password === null){
        loading.dismiss();
        return null;
    }
    else{
        return new Promise((resolve, reject) => {

            this.http.post(this.configure.getUrl()+"register", {
              "name":credentials.name+"",
              "email":credentials.email+"",
              "lastname":credentials.lastname,
              "password":credentials.password+"",
              "telefono":credentials.phone+""

            }, {
            })
            .subscribe((res:any) => {
              var usr:User=new User;

              usr.id=res.user.id;
              usr.email=res.user.email;
              usr.name=res.user.name;
              usr.lastname=res.user.lastname;
              usr.phone=res.user.telefono;
              usr.access_token=res.access_token;

              this.storage.set('user',usr).then((user:User)=>{
                this.event.publish('user:created',true);
                this.showToast("Hola "+user.name)
                this.storage.get('cart').then(async (cart:Cart)=>{
                    if(cart.quantity>0){
                      let productsCarted:ProductsCarted[]=cart.products;
                      for(let value of productsCarted){
                        await this.cartService.addProductsBackground(value.product,value.count,user);
                      }
                      
                      this.cartService.getCartOnly(user)
                      .subscribe((res) => {
                        loading.dismiss();
                        this.cartService.getUserCartSaved(res);
                      });
                      
                    }
                    else{
                      this.cartService.getCartOnly(user)
                      .subscribe((res) => {
                        loading.dismiss();
                        this.cartService.getUserCartSaved(res);
                      });
                    }
                  });
                // this.cartService.getCartOnly(user)
                // .subscribe(res => {
                //   this.cartService.getUserCartSaved2(res).then((value)=>{
                    
                //   });
                // });
                this.fcmService.getToken().then(token=>{
                  this.registerFirebaseToken(user,token);
                }).catch(e=>{
                  // alert(e);
                });
              });
              
              // loading.dismiss();
            }, (err) => {
              reject(err);
              loading.dismiss();
              this.showToast("Error...");              
            });

        });
    }
    

  


}

  async login(credentials){
    
    const loading=await this.loadingCtrl.create({
      message:'Verificando identidad...'
    });
    await loading.present();

    if(credentials.name === null || credentials.email === null || credentials.password === null){
        loading.dismiss();
        return null;
    }
    else{
        return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json' );

            this.http.post(this.configure.getUrl()+"login",
            {
                "email":credentials.email+"",
                
                "password":credentials.password+""
            },
            {
                
             }
            
            ).subscribe((res:any) => {

              var rol:string=res.user.roles[0].name;
              if(rol==="client"){
                var usr:User=new User;

                usr.id=res.user.id;
                usr.email=res.user.email;
                usr.name=res.user.name;
                usr.lastname=res.user.lastname;
                usr.phone=res.user.telefono;
                usr.access_token=res.access_token;

                this.storage.set('user',usr).then((user:User)=>{
                  this.event.publish('user:entered',true);
                  this.showToast("Hola "+user.name);

                  // this.storage.get('cart').then(async (cart:Cart)=>{
                  //   if(cart.quantity>0){
                  //     let productsCarted:ProductsCarted[]=cart.products;
                  //     for(let value of productsCarted){
                        
                        
                  //       await this.cartService.addProductsBackground(value.product,value.count,user);
                  //     }
                      
                  //     this.cartService.getCartOnly(user)
                  //     .subscribe((res) => {
                  //       loading.dismiss();
                  //       this.cartService.getUserCartSaved(res);
                  //     });
                      
                  //   }
                  //   else{
                  //     this.cartService.getCartOnly(user)
                  //     .subscribe((res) => {
                  //       loading.dismiss();
                  //       this.cartService.getUserCartSaved(res);
                  //     });
                  //   }
                  // });

                  this.cartService.getCartOnly(user)
                  .subscribe((res) => {
                    this.cartService.getUserCartSaved2(res).then(async ()=>{
                      let carrito:Cart = await this.cartService.getUserCartSaved3();
                      
                      for(let val of carrito.products){
                        
                        await this.cartService.addProductToCartBackground(val.product,val.count,user);
  
                      }

                      this.storage.get('cart2').then((cart:Cart)=>{
                        this.storage.set('cart',cart).then(()=>{
                          loading.dismiss();
                          this.event.publish("cart:saved",true);
                          this.storage.remove('cart2');
                        });
                      });
                    });
                    
                    // this.cartService.getUserCartSaved2(res).then(async (value:Cart)=>{
                    //   for(let val of value.products){
                        
                        
                    //     // await this.cartService.addProductToCartBackground(val.product,val.count,user);
                    //   }

                      
                      
                    // });
                  });

                  
                  this.fcmService.getToken().then(token=>{
                    this.storage.set('firebase-token',token).then((data)=>{
                      this.registerFirebaseToken(user,token);
                    });
                    
                  }).catch(e=>{
                  });
                });

              }
              else{
                this.showToast("Lo sentimos, no está autorizado.");
              }
              
              
              
              // loading.dismiss();
                
            }, (err) => {
                reject(err);
                loading.dismiss();
                this.showToast("Error, intente de nuevo.");
            });

            

        });
    }

  }

  registerFirebaseToken(user:User,token:string){

    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );

      this.http.post(this.configure.getUrl()+"user/"+user.id+"/token",
      {
          "firebase_token":token
      },
      {
        headers: new HttpHeaders({
          'Accept': 'application/json;charset=utf-8',
          'Content-Type': 'application/json;charset=utf-8',
          'Authorization': 'Bearer '+user.access_token
          })
       }
      
      ).subscribe((res:any) => {
        
       
          
      }, (err) => {
        alert(JSON.stringify(err));
          reject(err);
          this.showToast(err);
      });

      

  });

  }

  async showToast(msj:string){
        
    const DeclinedToast =await  this.toastCtrl.create({
        message: msj,
        duration: 4000,
        position: 'bottom',
        buttons: [ {
            text: 'Cerrar',
            role: 'cancel'
          }
        ]
    });
    DeclinedToast.present();
  }

  async logout(user:User){
    const loading=await this.loadingCtrl.create({
      message:'Cerrando servicios...'
    });
    await loading.present();

    return new Promise(async (resolve, reject) => {

      this.storage.get('firebase-token').then((token)=>{
        this.http.post(this.configure.getUrl()+"logout",
        {
          "firebase_token":token
        },
        {
          headers: new HttpHeaders({
            'Accept': 'application/json;charset=utf-8',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Bearer '+user.access_token
            })
         }
        
        ).subscribe((res:any) => {
  
         if(res){
            if(res.message=="Successfully logged out"){
              this.showToast("Hasta luego "+user.name);
              
              this.storage.clear().then(()=>{
                this.cartService.getCartOnly(null)
                .subscribe(res => {
                  this.firebase.signOut();
                  this.event.publish('user:loggedout',true);
                  this.load();
                });
                
              });
            }
          }
          loading.dismiss();
        }, (err) => {
            reject(err);
            this.showToast(err);
        });
      });

      

      

  });
  }

  load(){
    this.apiService.getData(null)
      .subscribe(res => {
        Promise.all([
          this.saveData("publicidads",res[0].publicidads),
          this.saveData("categorias",res[0].categorias),
          this.saveData("favoritos",res[0].favoritos),
          this.saveData("productos",res[0].productos),
          this.saveData("establecimientos",res[0].establecimientos),
          this.saveData("top10",res[0].top10)
        ]).then(values => { 
          
          this.event.publish("savedData",true);
        });
      }, err => {
        this.showToast(err.error);
      });
  }

  saveData(key:string,dat){

    return new Promise((resolve, reject) => { 
      this.storage.set(key,dat).then(()=>{
        resolve("Collected!!");
      });
    });
  }

  
}
