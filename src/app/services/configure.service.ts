import { Injectable } from '@angular/core';

@Injectable()
export class ConfigureService {

  private url: string = 'https://prontoapi.vitiks.com/api/api/';
  // private url: string = 'http://localhost/pronto-back-end/public/api/';


  constructor() { }

  getUrl() {
		return this.url;
	}
}
