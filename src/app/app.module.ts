import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiServiceService } from './services/api-service.service';
import { AuthService } from './services/auth.service';
import { ConfigureService } from './services/configure.service';
import { CartServiceService } from './services/cart-service.service';
import { DireccionServiceService } from './services/direccion-service.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

import { HTTP } from '@ionic-native/http/ngx';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { Geolocation } from '@ionic-native/geolocation/ngx';

import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

import { AgmCoreModule } from '@agm/core';
import { IonicStorageModule } from '@ionic/storage';
import { SearchService } from './services/search';
import { FCM } from '@ionic-native/fcm/ngx';
import { FCMService } from './services/fcm.service';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { SmsService } from './services/sms.service';
import { IonicImageLoader } from 'ionic-image-loader';
import { WebView } from '@ionic-native/ionic-webview/ngx';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey:
      'AIzaSyAt7Mk0F78UZ5Oj33N0IPpZE8LgauIugYM'
      
    }),
    IonicImageLoader.forRoot(),

  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Diagnostic,
    HTTP,
    Geolocation,
    LocationAccuracy,
    ApiServiceService,
    AuthService,
    CartServiceService,
    ConfigureService,
    DireccionServiceService,
    SearchService,
    FCM,
    FCMService,
    FirebaseAuthentication,
    SmsService,
    WebView
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
