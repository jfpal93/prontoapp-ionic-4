import { Component } from '@angular/core';

import { Platform, LoadingController, ToastController, Events, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { ApiServiceService } from './services/api-service.service';
import { Storage } from '@ionic/storage';
import { AuthService } from './services/auth.service';
import { User } from './models/user';
import { CartServiceService } from './services/cart-service.service';
import { DireccionServiceService } from './services/direccion-service.service';
import { Router } from '@angular/router';
import { Cart } from './models/cart';
import { ProductsCarted } from './models/productsCarted';
import { Producto } from './models/producto';
import { FCMService } from './services/fcm.service';
import { OrderConfirmationPage } from './pages/order-confirmation/order-confirmation.page';
import { ImageLoaderConfigService } from 'ionic-image-loader';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  private sub1$:any;
  constructor(
    public loadingController:LoadingController,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private locationAccuracy:LocationAccuracy,
    private diagnostic:Diagnostic,
    private storage:Storage,
    public toastCtrl:ToastController,
    private loginService:AuthService,
    private dirService:DireccionServiceService,
    private events:Events,
    private apiService:ApiServiceService,
    
    private cartService:CartServiceService,
    private router:Router,
    private fcmService:FCMService,
    private imageLoaderConfig:ImageLoaderConfigService


  ) {
    
    this.load();
    this.initializeApp();
    
    // disable spinners by default, you can add [spinner]="true" to a specific component instance later on to override this
    // enable debug mode to get console logs and stuff
    // imageLoaderConfig.enableDebugMode();
    // set a fallback url to use by default in case an image is not found
    // imageLoaderConfig.setFallbackUrl('assets/fallback.png');
    imageLoaderConfig.enableSpinner(true);

    imageLoaderConfig.setImageReturnType('base64');

    imageLoaderConfig.setSpinnerColor('secondary');
    imageLoaderConfig.setSpinnerName('bubbles');


    imageLoaderConfig.maxCacheSize = 100 * 1024 * 1024; // 2 MB
    // imageLoaderConfig.maxCacheAge = 60 *÷ 1000; // 1 minute
  }

  saveData(key:string,dat){

    return new Promise((resolve, reject) => { 
      this.storage.set(key,dat).then(()=>{
        resolve("Collected!!");
      });
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      //ios
      // this.diagnoseGeolocation();
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#191919');
      this.reciveNotifications();
      this.router.navigateByUrl("tabs/home");

      this.events.subscribe('savedData',(bool)=>{

        if(bool){
          setTimeout(() => {
            this.splashScreen.hide();
            this.events.publish("initapp",true);
          }, 700);
        }

      });   
      this.platform.resume.subscribe(() => {
        this.diagnoseGeolocation();
        this.load();
       }); 

    });
    
  }

  ionViewWillUnload() {
    this.sub1$.unsubscribe();
  }

  async load(){
    const loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await loading.present();
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.apiService.getData(data)
        .subscribe( (res) => {
          
          loading.dismiss();
          if(res[2].length>0){
            if(res[2][0][0]){
              this.storage.set('orderConfirmationData',JSON.stringify(res[2][0][0])).then(()=>{
                this.events.publish('Order:Confirmation',true);
              });
            }
          }
          
          
          Promise.all([
            this.saveData("publicidads",res[0].publicidads),
            this.saveData("categorias",res[0].categorias),
            this.saveData("favoritos",res[0].favoritos),
            this.saveData("productos",res[0].productos),
            this.saveData("establecimientos",res[0].establecimientos),
            this.saveData("top10",res[0].top10),
            this.saveData("coordenadas",res[0].coordenadas)
          ]).then(values => { 
            loading.dismiss();
            this.events.publish("savedData",true);
          });
          
          
          let cart=new Cart();
          cart.id=res[1].carrito_id;
          
          let prods:ProductsCarted[]=[];
          for(var prod of res[1].products){
            let p = new Producto;
            p.id = prod.id;
            p.linkImagen=prod.linkImagen;
            p.nombre=prod.nombre;
            p.precio=prod.precio;
            p.disponible=prod.disponible;
            p.descripcion=prod.descripcion;
            p.establecimiento_id=prod.establecimiento_id
            p.nombreEstablecimiento=prod.establecimiento

            let pc = new ProductsCarted;
            pc.product=p;
            pc.count=prod.cant;

            prods.push(pc);

          }
          cart.products=prods;
          cart.quantity=res[1].cant;
          cart.state="idle";
          cart.subtotal=res[1].total;
          cart.total=res[1].total;
          this.storage.set('cart', cart).then(()=>{
              this.events.publish("cart:saved",true);
          }); 
          
          
  
          
  
          
        }, err => {
          loading.dismiss();
          this.showToast(err.message);
          
        });
        
      }
      else{
        this.apiService.getData(data)
        .subscribe( (res) => {
          loading.dismiss();
          Promise.all([
            this.saveData("publicidads",res[0].publicidads),
            this.saveData("categorias",res[0].categorias),
            this.saveData("favoritos",res[0].favoritos),
            this.saveData("productos",res[0].productos),
            this.saveData("establecimientos",res[0].establecimientos),
            this.saveData("top10",res[0].top10),
            this.saveData("coordenadas",res[0].coordenadas)
          ]).then(values => { 
            
            this.events.publish("savedData",true);
  
              // this.splashScreen.hide();

              // alert(JSON.stringify(values));
              // setTimeout(() => {
              //   this.splashScreen.hide();
              //   this.events.publish("savedData",true);
              // }, 700);
          });
        }, err => {
          this.showToast(err);
          loading.dismiss();
        });
      }
      
    });
  }

  

  diagnoseGeolocation(){
    
    this.diagnostic.isLocationEnabled().then((isAvailable)=>{      
      if(!isAvailable){
        this.locationAccuracy.canRequest().then(()=>{
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
              (data) => {
              },
              error => {
              }
            );
        }).catch((e)=>{
        });
      }
      else{
        this.diagnostic.isLocationAuthorized().then((data:boolean)=>{
          if(!data){
            this.diagnostic.requestLocationAuthorization();
          }
        });
      }
    }).catch( (e) => {
    });
  }

  

  async showToast(msj:string){
        
    const DeclinedToast =await  this.toastCtrl.create({
        message: msj,
        duration: 4000,
        position: 'bottom',
        buttons: [ {
            text: 'Cerrar',
            role: 'cancel'
          }
        ]
    });
    DeclinedToast.present();
  }

  diagnoseLocation(){

    this.diagnostic.isLocationEnabled().then((isAvailable)=>{
      if(!isAvailable){
        this.locationAccuracy.canRequest().then((canRequest:boolean)=>{
          if(canRequest){
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
              () =>{},
              error => alert(error)
            );
          }
          else{
            
          }
        }).catch((e)=>{
          this.showToast(e);
        })
      }

    }).catch( (e) => {
      this.showToast(e);
    });

  }

  reciveNotifications(){
    this.fcmService.getNotification().subscribe((data:any) => {
      if (data.wasTapped) {
        // this.showToast('Recieved!!');
        // // this.showConfirmationPage(data);
        // if(data.confirmation){
        //   this.storage.set('orderConfirmationData',data.pedido).then(()=>{
        //     this.events.publish('Order:Confirmation',true);
        //   });
        // }
        // if(data.arrived){
        //   this.storage.set('orderData',data.pedido).then(()=>{
        //     this.events.publish('Order:arrived',true);
        //   });
        // }
        
        
      } else {
        if(data.confirmation){
          this.storage.set('orderConfirmationData',data.pedido).then(()=>{
            this.events.publish('Order:Confirmation',true);
          });
        }
        if(data.arrived){
          this.storage.set('orderData',data.pedido).then(()=>{
            this.events.publish('Order:arrived',true);
          });
          
        }
        
      }
    });
  }

  
  // diagnoseGeolocation(){
  //   // this.requestAUTH();
    
  //   this.diagnostic.isLocationEnabled().then((isAvailable)=>{
      
  //     if(!isAvailable){
  //       this.locationAccuracy.canRequest().then((canRequest:boolean)=>{
  //         if(canRequest){
  //           this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
  //             () => {
  //               // this.getLocation();
                
  //             },
  //             error => {
  //               // this.lat="";
  //               // this.getLocation();
  //             }
  //           );
  //         }
  //         else{
            
  //         }
  //       }).catch((e)=>{
  //       })
  //     }

  //   }).catch( (e) => {
  //     // alert(e);
  //   });
  // }
  

  
}
