import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, Events, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PedidosService } from 'src/app/services/pedidos.service';

@Component({
  selector: 'app-order-arrived',
  templateUrl: './order-arrived.page.html',
  styleUrls: ['./order-arrived.page.scss'],
})
export class OrderArrivedPage implements OnInit {

  private data:any;
  shownGroup=null;

  codigo:string;

  total:number;

  products:any[]=[];


  constructor(
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private events:Events,
    private storage:Storage,
    private pedidoService:PedidosService,
    private platform:Platform

  ) { 
    this.data=JSON.parse(this.navParams.get("data"));
    this.codigo=this.data.orden_codigo;
    this.products=this.data.products;
    this.total=this.data.total;

    this.platform.resume.subscribe(async () => {
      await this.modalCtrl.dismiss();
     }); 
  }

  ngOnInit() {
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  }

  isGroupShown(group) {
      return this.shownGroup === group;
  }

  async onClose(){
    await this.modalCtrl.dismiss();
    // this.modalCtrl.dismiss({
    //   'dismissed': true
    // });
  }

}
