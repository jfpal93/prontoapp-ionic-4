import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderArrivedPage } from './order-arrived.page';

describe('OrderArrivedPage', () => {
  let component: OrderArrivedPage;
  let fixture: ComponentFixture<OrderArrivedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderArrivedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderArrivedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
