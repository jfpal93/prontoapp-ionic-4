import { Component, OnInit } from '@angular/core';
import { Events, NavController, Platform } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  credentials = { 
    email: '', 
    password: '' };

    private scanModalPage:boolean;

  constructor(
    private event:Events,
    private auth:AuthService,
    public navCtrl: NavController, 
    private router:Router,
    private platform:Platform
  ) {
    
    this.platform.backButton.subscribeWithPriority(1,()=>{
      // alert(value);
      this.event.publish("BackTo:NotLoggedIn",true);
    });
   }

  ngOnInit() {
  }

  onClose(){
    this.event.publish("BackTo:NotLoggedIn",true);
  }

  ionViewWillLeave(){
    // this.event.publish("BackTo:NotLoggedIn",true);
  }

  ionViewWillEnter() {
  }

  login(){
    this.auth.login(this.credentials);
  }

  openRegister(){
    this.navCtrl.navigateForward("/register");
  }

}
