import { Component, OnInit } from '@angular/core';
import { Events, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  user:User;

  disabled=true;

  name:string="";
  lastname:string="";
  phone:string="";
  email:string="";

  constructor(
    private storage:Storage,
    private navCtrl:NavController,
    private event:Events,
    private authService:AuthService
  ) {

    this.event.subscribe('user:entered',(bool)=>{
      if(bool){
        this.getUserData();
      }
      
    });

    this.event.subscribe('user:created',(bool)=>{
      if(bool){
        this.getUserData();
      }
      
    });

    this.event.subscribe('user:loggedout',(bool)=>{
      if(bool){
        this.name="";
        this.lastname="";
        this.phone="";
        this.email="";
      }
      
    });
   }

  ngOnInit() {
    

    

    
  }

  ionViewWillEnter(){
    this.getUserData();
  }

  getUserData(){
    this.storage.get("user").then((data:User)=>{
      if(data){
        this.user=data;
        this.name=this.user.name;
        this.lastname=this.user.lastname;
        this.phone=this.user.phone;
        this.email=this.user.email;
      }

    });
  }

  signout(){
    this.storage.get('user').then((data:User)=>{
      
      this.authService.logout(data);
    });
  }

  editData(){
    this.disabled=false;
  }

  cancel(){
    this.disabled=true;
  }

  save(){
    alert(this.email);
    
    this.disabled=true;
  }

}
