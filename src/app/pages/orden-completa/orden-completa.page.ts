import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, Events, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-orden-completa',
  templateUrl: './orden-completa.page.html',
  styleUrls: ['./orden-completa.page.scss'],
})
export class OrdenCompletaPage implements OnInit {

  total:any;

  codigo:any;

  constructor(
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private events:Events,
    private storage:Storage,
    private platform:Platform

  ) {
    // this.total=this.navParams.get("total");

    this.storage.get('codigo-orden').then((data:any)=>{
      this.total=data.total;
      this.codigo=data.codigo;
      
    });
    this.platform.resume.subscribe(async () => {
      await this.modalCtrl.dismiss();
     }); 
   }

  ngOnInit() {
  }

  onClose(){
    // this.modalCtrl.dismiss();
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  ionViewWillLeave(){
    this.events.publish("order:placed",true);
    this.storage.remove('codigo-orden');
  }

}
