import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenCompletaPage } from './orden-completa.page';

describe('OrdenCompletaPage', () => {
  let component: OrdenCompletaPage;
  let fixture: ComponentFixture<OrdenCompletaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdenCompletaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdenCompletaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
