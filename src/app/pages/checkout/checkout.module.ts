import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutPage } from './checkout.page';
import { AgmCoreModule } from '@agm/core';
import { DirOptionsComponent } from 'src/app/components/dir-options/dir-options.component';
import { OrdenCompletaPage } from '../orden-completa/orden-completa.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey:
      'AIzaSyAt7Mk0F78UZ5Oj33N0IPpZE8LgauIugYM'
      
    })
  ],
  declarations: [CheckoutPage, DirOptionsComponent,OrdenCompletaPage],
  entryComponents:[DirOptionsComponent,OrdenCompletaPage]
})
export class CheckoutPageModule {}
