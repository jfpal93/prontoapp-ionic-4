import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Cart } from 'src/app/models/cart';
import { Direccion } from 'src/app/models/direccion';
import { DireccionServiceService } from 'src/app/services/direccion-service.service';
import { Events, NavController, AlertController, ModalController, IonSlides, PopoverController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { User } from 'src/app/models/user';
import { OrdenCompletaPage } from '../orden-completa/orden-completa.page';
import { AddDirectionPage } from 'src/app/add-direction/add-direction.page';
import { DirOptionsComponent } from 'src/app/components/dir-options/dir-options.component';
import { CartServiceService } from 'src/app/services/cart-service.service';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {

  cart:Cart=new Cart;

  private selectedAddressId:number;

  iconMap={
    url:"../../assets/imgs/pin.svg",
    scaledSize: {
      width: 20,
      height:20

    }
  }

  slideDir = {
    initialSlide: 0,
    spaceBetween: 0,
    slidesPerView:1.04
  };
  

  styles = [{
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }];

  private addressSelectedDB:boolean=false;
  private direccion:any;

  dirArray:Direccion[];
  private firstDir:Direccion= new Direccion;

  ready:boolean=true;

  private directions:any=[
    {
      label: 'Ipanema',
      value: 'Ipanema'
    },
    {
      label: 'Alborada',
      value: 'Alborada'
    }
  ]

  private total:number=0;

  @ViewChild('addressSlide', {static: false}) addressSlide: IonSlides;

  constructor(
    private router:Router,
    private dirService:DireccionServiceService,
    private events:Events,
    private storage:Storage,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private modalCtrl:ModalController,
    private route: ActivatedRoute,
    private popoverController: PopoverController,
    private cartService:CartServiceService,
    private diagnostic:Diagnostic,
    private platform:Platform,
    private locationAccuracy:LocationAccuracy,
    private toastCtrl:ToastController
  ) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.cart = this.router.getCurrentNavigation().extras.state.cart;
      }
    });

    

    
    

    this.getDirecciones();
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.loadDirecciones();
  }

  loadDirecciones(){
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.dirService.getDirecciones(data);

      }      
    });
  }

  onClose(){
    this.router.navigateByUrl('/cart');
  }

  async onSelectAddress(){

    const theNewInputs = [];
    for (let i = 0; i < this.directions.length; i++) {
      theNewInputs.push(
        {
          type: 'radio',
          label: this.directions[i].label,
          value: this.directions[i].value,
        }
      );
    }

    const alert = await this.alertCtrl.create({
      header: 'Seleccionar Direccion de envio.',
      inputs: theNewInputs,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Listo',
          handler: data => {
            this.addressSelectedDB=true;
            this.direccion=data;
            this.ready=false;
          }
        }
      ]
    });
    await alert.present();

  }

  async onAddAddress(){
    const prompt = await this.alertCtrl.create({
      header: 'Nueva Dirección',
      message: "Agregar nueva dirección de envío",
      inputs: [
        {
          name: 'direccion',
          placeholder: 'Urb, Calle, Mz/Villa'
        },
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Guardar',
          handler: data => {
            
            let dir={
              label: data.direccion,
              value: data.direccion
            }
            this.directions.push(dir);
          }
        }
      ]
    });
    await prompt.present();

  }

  async placeOrder(){
    // this.onClose();
    
    // this.storage.set("order",this.cart);
    // this.storage.remove("cart");
    
    this.storage.get('user').then((user:User)=>{
      this.cartService.placeOrder(user,this.selectedAddressId).then(()=>{
        
        
      });
    });

    const modal = await this.modalCtrl.create({
      component:OrdenCompletaPage,
      componentProps:{
        total:this.cart.total
      }
    });

    this.events.subscribe('placedOrder',async (bool)=>{
      if(bool){
        
        modal.present();
      }
    });
    const { data } = await modal.onWillDismiss();
    if(data.dismissed){
      this.events.unsubscribe('placedOrder');
    }
    
  }

  onError(error) {
      alert("The following error occurred: " + JSON.stringify(error));
  }

  requestLocationAccuracy() {
    // const locAuthorized = await this.diagnostic.isLocationAuthorized();
    // const reqLocAuthorization = await this.diagnostic.requestLocationAuthorization();

    // alert(locAuthorized);
    // alert(reqLocAuthorization);
    this.diagnostic.getLocationAuthorizationStatus().then((value)=>{
      // alert("Get status"+value);
      
      this.handleLocationAuthorizationStatus(value);
    }).catch((error)=>{
      this.onError(error);
    });
  }

  async requestLocationAuthorization() {
    await this.diagnostic.requestLocationAuthorization()
    // .then((value)=>{
      // alert("request status"+value);
      // this.handleLocationAuthorizationStatus(value);
    this.requestLocationAccuracy2();
    // }).catch((error)=>{
    //   this.onError(error);
    // });
  }

  requestLocationAccuracy2() {
    // const locAuthorized = await this.diagnostic.isLocationAuthorized();
    // const reqLocAuthorization = await this.diagnostic.requestLocationAuthorization();

    // alert(locAuthorized);
    // alert(reqLocAuthorization);
    this.diagnostic.getLocationAuthorizationStatus().then((value)=>{
      // alert("Get status"+value);
      
      this.handleLocationAuthorizationStatus2(value);
    }).catch((error)=>{
      this.onError(error);
    });
  }

  handleLocationAuthorizationStatus2(status) {
    // alert(status)
    switch (status) {
        case "GRANTED":
            this._makeRequest();
            
            break;
        case "DENIED":
            this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
            // this.closeModal();
            break;
        case "DENIED_ONCE":
            this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
            // this.closeModal();
            break;
            
        case "DENIED_ALWAYS":
            // Android only
            this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
            // this.closeModal();
            break;
        case "GRANTED_WHEN_IN_USE":
            // iOS only
            this.onError("Location services is already switched ON");
            break;
    }
  }


  handleLocationAuthorizationStatus(status) {
    if(this.platform.is('ios')){
      switch (status) {
        case "authorized":
            this.router.navigateByUrl('/add-direccion');
            break;
        case "authorized_when_in_use":
            this.router.navigateByUrl('/add-direccion');
            break;
        case "denied_always":
            this.showDialog();
            break;
        case "denied":
            this.showDialog();
            break;
        case "restricted":
            this.showDialog();
            break;
        case "not_determined":
            this.showDialog();
            break;
      }
    }

    if(this.platform.is('android')){
      switch (status) {
        case "GRANTED":
          this._makeRequest();
          
          break;
        case "DENIED":
          this.diagnoseGeolocation();
          // this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
          // this.closeModal();
          break;
        case "DENIED_ONCE":
          // this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
          // this.closeModal();
          this.diagnoseGeolocation();
          break;
            
        case "DENIED_ALWAYS":
          // Android only
          // this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
          // this.closeModal();
          this.diagnoseGeolocation();
          break;
        case "GRANTED_WHEN_IN_USE":
          this.diagnoseGeolocation();
          // iOS only
          // this.onError("Location services is already switched ON");
          break;
        case "NOT_REQUESTED":
          this.diagnoseGeolocation();
          break;
      }

    }
    
  }

  async showDialog(){
    const alert = await this.alertCtrl.create({
      header: '¡HEY!',
      message: 'Necesitamos permiso para acceder a la información de GPS para darte la mejor experiencia a domicilio. Puedes hacerlo en: Configuración/Privacidad/Localización/Pronto Milagro',
      buttons: [ {
        text: 'Ok'
      }
    ]
    });

    await alert.present();
  }

  goToSettings(){
    this.diagnostic.switchToSettings().then(()=>{
      this.diagnoseGeolocation();

    });
  }

  diagnoseGeolocation(){
    
    this.diagnostic.isLocationEnabled().then((isAvailable)=>{      
      if(!isAvailable){
        this.locationAccuracy.canRequest().then(()=>{
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
              (data) => {
                alert(JSON.stringify(data))
              },
              error => {
              }
            );
        }).catch((e)=>{
        });
      }
      else{
        this.diagnostic.isLocationAuthorized().then((data:boolean)=>{
          if(!data){
            this.diagnostic.requestLocationAuthorization().then((data)=>{
              this.handleLocationAuthorizationStatus2(data);
            });
          }
        });
      }
    }).catch( (e) => {
    });
  }

  _makeRequest(){
    this.locationAccuracy.canRequest().then((canRequest)=>{
        if (canRequest) {
          if(this.platform.is('ios')){
            this.router.navigateByUrl('/add-direccion');
          }
          else{
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
              () => {
                // this.getLocation();
                this.router.navigateByUrl('/add-direccion');

              },
              error => {
                this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
                // this.closeModal();
              }
            );
          }
          
        } 
    });
  }


  addNewDirection(){
    // const modal = await this.modalCtrl.create({
    //   component:AddDirectionPage
    // });
    // modal.present();
    // this.router.navigateByUrl('/add-direccion');
    this.requestLocationAccuracy();
    // this._makeRequest();
  }

  returnString(coord:any){
    return parseFloat(coord);
  }

  getDirecciones(){
    this.events.subscribe('user:direcciones',(bool)=>{
      if(bool){
        this.storage.get('direcciones').then((data:Direccion[])=>{
          if(data)
          {
            data=this.parseToFloat(data);
            this.dirArray=data;
            let listOfSelectedAdresses:Direccion[]=[];

            this.dirArray.forEach(function (value:Direccion) {
                if(value.selected){
                  listOfSelectedAdresses.push(value);
                }
            });

            if(listOfSelectedAdresses.length==1){
              this.ready=false;
              this.addressSlide.slideTo(0);
              
              this.selectedAddressId=listOfSelectedAdresses[0].id;
            }
            else{
              this.ready=true;
            }
          }
          
        });

      }
      
    });
  }

  parseToFloat(data:Direccion[]){
      data.forEach(function (value:Direccion) {
        value.lat=parseFloat(value.lat+"");
        value.lng=parseFloat(value.lng+"");
        value.selected=!!+value.selected;
    });
    return data;

  }

  datachanged(event:any,item:Direccion){
    if(!event["target"].checked){
      this.changeToTrue(item.id);
    }
    else{
      this.changeToFalse(item.id);
    }
  }

  changeToTrue(id:any){
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.dirService.selectDireccion(data,id);
      }
      
    });
  }

  changeToFalse(id:any){
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.dirService.deselectDireccion(data,id);
      }
      
    });
  }

  changeAllFalse(){
      this.dirArray.forEach(function (value:Direccion) {
          value.selected=false;

    });

  return this.dirArray;

  }

  async dirPopover(ev: any,dir:Direccion) {
    const popover = await this.popoverController.create({
      component: DirOptionsComponent,
      event: ev,
      componentProps: { direccion: dir },
      cssClass: 'popover_class',
    });

   
    await popover.present();
  }

  async showToast(msj:string){
          
    const DeclinedToast =await  this.toastCtrl.create({
        message: msj,
        duration: 4000,
        position: 'bottom',
        buttons: [ {
            text: 'Cerrar',
            role: 'cancel'
          }
        ]
    });
    DeclinedToast.present();
  }

  
}
