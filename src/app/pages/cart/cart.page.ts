import { Component, OnInit } from '@angular/core';
import { Events } from '@ionic/angular';
import { Categoria } from 'src/app/models/categoria';
import { Cart } from 'src/app/models/cart';
import { ProductsCarted } from 'src/app/models/productsCarted';
import { Storage } from '@ionic/storage';
import { Producto } from 'src/app/models/producto';
import { Establecimiento } from 'src/app/models/establecimiento';
import { CartServiceService } from 'src/app/services/cart-service.service';
import { User } from 'src/app/models/user';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  private currentNumber = 0;

  private categories:Categoria[]=[];
  private establecimientos:Establecimiento[]=[];
  cart:Cart=new Cart;

  cartProds:ProductsCarted[]=[];

  emptyCart:boolean=false;

  constructor(
    private event:Events,
    private storage:Storage,
    private cartService:CartServiceService,
    private router:Router

  ) { 
    this.storage.get("categorias").then((data:Categoria[])=>{
      this.categories=data; 
    });
    this.storage.get("establecimientos").then((data:Establecimiento[])=>{
      this.establecimientos=data; 
    });
    this.storage.get('cart').then((data:Cart)=>{
      if(data)
      {
        this.currentNumber=data.quantity;
        this.cart=data;
        this.cartProds=data.products;
        if(this.currentNumber==0){
          this.emptyCart=true;
        }
      }
      else{
        // this.currentNumber=data.quantity;
      }
    });

    this.updateCartData();
  }

  ngOnInit() {
  }

  onClose(){
    // this.event.publish("cart:closed",true);
    
  }

  returnCategoryByID(product:Producto){
    for(var cat of this.categories){
      for(var estab of this.establecimientos){
        if(estab["categoria_id"] == cat["id"]){
          if(product.establecimiento_id == estab["id"]){
            return cat["nombre"];
          }
          
        }
      }
      
    }
  }

  onAdd(prod:Producto){
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.cartService.addProductToCart(prod,1,data);
      }
      else{
      this.cartService.addProductToCart(prod,1,data);
      }
    });
    
  }

  onDelete(prod:Producto){

    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.cartService.deleteProductToCart(prod,data);
      }
      else{
        this.cartService.deleteProductToCart(prod,data);
      }
    });
    
  }

  updateCartData(){

    this.event.subscribe('cartUpdate:addedProduct',(bool:boolean)=>{
      if(bool){
        this.storage.get("cart").then((data:Cart)=>{
          this.cart=data; 
          this.cartProds=data.products;
          
        });
      }
      
      
    });

    this.event.subscribe('cartUpdate:deletedProduct',(bool:boolean)=>{
      if(bool){
        this.storage.get("cart").then((data:Cart)=>{
          this.cart=data; 
          this.cartProds=data.products;
          this.currentNumber=data.quantity;
          if(this.currentNumber==0){
            this.emptyCart=true;
          }
        });
      }
      
      
    });

  }

  checkOut(){
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        let navigationExtras: NavigationExtras = {
          state: {
            cart: this.cart
          }
        };
        this.router.navigate(["/checkout"],navigationExtras);
      }
      else{
        this.router.navigateByUrl("/login");
      }
    });

  }

}
