import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/services/search';
import { Producto } from 'src/app/models/producto';
import { Categoria } from 'src/app/models/categoria';
import { Establecimiento } from 'src/app/models/establecimiento';
import { Storage } from '@ionic/storage';
import { Cart } from 'src/app/models/cart';
import { NavigationExtras, Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { Search } from 'src/app/models/search';
import { Events } from '@ionic/angular';
import { CartServiceService } from 'src/app/services/cart-service.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.page.html',
  styleUrls: ['./buscar.page.scss'],
})
export class BuscarPage implements OnInit {

  slideOptsSearch = {
    initialSlide: 0,
    spaceBetween: 4,
    slidesPerView:2.2
  };

  products:Producto[]=[];
  categorias:Categoria[]=[];
  establecimientos:Establecimiento[]=[];

  storageproducts:Producto[]=[];
  storagecategorias:Categoria[]=[];
  storageestablecimientos:Establecimiento[]=[];

  currentNumber = 0;

  searching:boolean=false;

  searchingTop10SearchByUser:boolean=false;
  searchingTop10:boolean=false;

  catSearch:boolean=false;
  prodSearch:boolean=false;
  estabSearch:boolean=false;

  private searchText:string="";

  top10Search:Search[]=[];

  top10SearchByUser:Search[]=[];


  myInput:string;

  constructor(
    private searchService:SearchService,
    private storage:Storage,
    private router:Router,
    private event:Events,
    private cartService:CartServiceService

  ) {
    
   }

  ngOnInit() {
    this.storage.get("categorias").then((data:Categoria[])=>{
      this.storagecategorias=data;
      this.categorias=data;
    });
    this.storage.get("establecimientos").then((data:Establecimiento[])=>{
      this.storageestablecimientos=data;
    });
    this.storage.get("productos").then((data:Producto[])=>{
      this.storageproducts=data;
    });

    this.storage.get("cart").then((data:Cart)=>{
      this.currentNumber=data.quantity;
    });

    // this.searchService.getTop10().subscribe((data:any)=>{
    //   this.top10Search=data.busqueda;
    // });

    this.storage.get('top10').then((data:Search[])=>{
      if(data)
      {
        this.top10Search=data;
        if(data.length>0){
          this.searchingTop10=true;
        }
        else{
          this.searchingTop10=false;
        }
        
      }
      // else{
      // //   const modal=this.modalCtrl.create(LoginPage);
      // // modal.present(); 
      //   this.top10SearchByUser=[]; 
      //   this.searchingTop10SearchByUser=false;
        
      // }
      
    });

    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.searchService.getTop10ByUser(data).subscribe((data:any)=>{
          this.top10SearchByUser=data.busqueda;
          if(this.top10SearchByUser.length>0){
            this.searchingTop10SearchByUser=true;
          }
          
        });
      }
    });

    this.event.subscribe('cart:saved',(bool)=>{
      if(bool){
        this.storage.get('cart').then((data:Cart)=>{
          if(data)
          {
            this.currentNumber=data.quantity;
          }
          else{
            // this.currentNumber=data.quantity;
          }
        });
      }
    });
    
    this.updateCartData();


  }

  detectSpaces(word:string){
    let splt = word.split(" ");
    if(splt.length == 1){
      return true;
    }
    return false;
  }

  onSearch(ev: any){
    
    this.searchText=ev.srcElement.value.toLowerCase();

    // let catFiltered=this.storagecategorias.filter((cat:Categoria)=>{
    let catFiltered=this.storagecategorias.filter((cat:Categoria)=>{
      return cat.nombre.toLowerCase().indexOf(ev.srcElement.value.toLowerCase())> -1;
    });
    let EstabFiltered=this.storageestablecimientos.filter((estab:Establecimiento)=>{
      return estab.nombre.toLowerCase().indexOf(ev.srcElement.value.toLowerCase())> -1;
    });
    let prodsFiltered=this.storageproducts.filter((prod:Producto)=>{
      return prod.nombre.toLowerCase().indexOf(ev.srcElement.value.toLowerCase())> -1 || prod.descripcion.toLowerCase().indexOf(ev.srcElement.value.toLowerCase())>-1;
    });

    var compEstabCat=this.storageestablecimientos.filter(o => this.storagecategorias.some(({id,nombre}) => o.categoria_id === id && nombre.toLowerCase().indexOf(this.searchText)> -1 ));
    var compEstabProd=this.storageproducts.filter(o => this.storageestablecimientos.some(({id,nombre}) => o.establecimiento_id === id && nombre.toLowerCase().indexOf(this.searchText)> -1 ));

    var compEstabCatProd=this.storageproducts.filter(o=>compEstabCat.some(({id})=>o.establecimiento_id === id))


    if(catFiltered.length > 0){
      this.categorias=catFiltered;
    }
    else{
      this.categorias=[];
    }

    if(prodsFiltered.length > 0 && compEstabProd.length === 0 && compEstabCatProd.length === 0){
      this.products=prodsFiltered;
    }
    if(compEstabProd.length > 0 && prodsFiltered.length === 0 && compEstabCatProd.length === 0){
      this.products=compEstabProd;
    }
    if(compEstabCatProd.length > 0 && prodsFiltered.length === 0 && compEstabProd.length === 0){
      this.products=compEstabCatProd;
    }
    if(prodsFiltered.length === 0 && compEstabProd.length === 0 && compEstabCatProd.length === 0){
      this.products=[];
    }

    if(EstabFiltered.length > 0 && compEstabCat.length === 0){
      this.establecimientos=EstabFiltered;   
    }
    if(compEstabCat.length > 0 && EstabFiltered.length === 0){      
      this.establecimientos=compEstabCat;   
    }
    if(EstabFiltered.length === 0 && compEstabCat.length === 0){
      this.establecimientos=[];
    }

    if(ev.srcElement.value===""){
      this.searching=false; 
      this.estabSearch=false; 
      this.prodSearch=false;
      this.catSearch=false;
      this.products=[];
      this.establecimientos=[];
      this.storage.get("categorias").then((data:Categoria[])=>{
        this.categorias=data;
      });
    }
    else{
      this.searching=true;     

      if(this.categorias.length ===0){
        this.catSearch=false;
      }
      else{
        this.catSearch=true;
      }
      if(this.products.length ===0){
        this.prodSearch=false;
      }
      else{
        this.prodSearch=true;
      }
      if(this.establecimientos.length ===0){
        this.estabSearch=false; 
      }
      else{
        this.estabSearch=true;  
      } 
    }
  }

  onCancel(ev: any){
    this.searching=false; 
    this.estabSearch=false; 
    this.prodSearch=false;
    this.catSearch=false;
    this.storage.get("categorias").then((data:Categoria[])=>{
      this.categorias=data;
    });

    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.searchService.getTop10ByUser(data).subscribe((data:any)=>{
          this.top10SearchByUser=data.busqueda;
          this.searchingTop10SearchByUser=true;
        });
      }
      else{
        this.top10SearchByUser=[]; 
        this.searchingTop10SearchByUser=false;
      }
    });
  }

  searchSelected(search:Search){
    this.myInput=search.palabra;
  }

  openProduct(p:Producto){
    this.saveSearch(p.nombre);
    let navigationExtras: NavigationExtras = {
      state: {
        product: p,
        buscar:true
      }
    };
    this.router.navigate(['/tabs/buscar/product'],navigationExtras)
  }

  openCategory(cat:Categoria){
    this.saveSearch(cat.nombre);
    let navigationExtras: NavigationExtras = {
      state: {
        category: cat,
        buscar:true
      }
    };
    this.router.navigateByUrl('/tabs/buscar/category',navigationExtras);
  }

  openEstab(estab:Establecimiento){
    this.saveSearch(estab.nombre);
    let navigationExtras: NavigationExtras = {
      state: {
        establecimiento: estab,
        buscar:true
      }
    };
    this.router.navigateByUrl('/tabs/buscar/see-more',navigationExtras);
  }

  saveSearch(text:any){
    if(this.searchText !==""){
      this.storage.get('user').then((data:User)=>{
        if(data)
        {
          this.searchService.saveSearch(text,data.id).subscribe((data)=>{
          });
        }
        else{
          this.searchService.saveSearch(text,null).subscribe((data)=>{
          });
        }
        
      });
    }
  }

  onAdd(prod:Producto){
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.cartService.addProductToCart(prod,1,data);
      }
      else{
      this.cartService.addProductToCart(prod,1,data);
      }
    });
  }

  onOpenShoppingCart(){
    this.router.navigateByUrl("/cart");
  }

  updateCartData(){
    this.event.subscribe('cartUpdate:addedProduct',(bool:boolean)=>{
      if(bool){
        this.storage.get("cart").then((data:Cart)=>{
          this.currentNumber=data.quantity;
        });
      }
    });
    this.event.subscribe('cartUpdate:deletedProduct',(bool:boolean)=>{
      if(bool){
        this.storage.get("cart").then((data:Cart)=>{
          this.currentNumber=data.quantity;
        });
      }
    });
  }
}
