import { Component, OnInit } from '@angular/core';
import { Events, NavController, Platform, LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { NavigationExtras, Router } from '@angular/router';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { SmsService } from 'src/app/services/sms.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerCredentials = { 
    name:"",
    email: "", 
    phone:"",
    lastname:"",
    password:'' };

    stripeForm:any;

  constructor(
    public event:Events,
    private auth:AuthService,
    public navCtrl: NavController,
    private platform:Platform,
    private router:Router,
    private smsService:SmsService,
    private loadingCtrl:LoadingController
  ) {
    
   }

  ngOnInit() {
  }

  async register(){
    const loading=await this.loadingCtrl.create({
      message:'Enviando mensaje...'
    });
    await loading.present();
    // this.auth.signup(this.registerCredentials);
    let smsver=this.smsService.sendSMS(this.registerCredentials.phone.substr(1));
    // alert(smsver);
    
    smsver.then((res:any)=>{
      loading.dismiss();
      let navigationExtras: NavigationExtras = {
        state: {
          credentials: this.registerCredentials,
          fireID:res
        }
      };
      this.router.navigate(["/smsconfirmation"],navigationExtras)
      // .then(()=>{
      //   
      // });
    }).catch((error: any)=>{
      loading.dismiss();
      alert(JSON.stringify(error));
    });
      
      
    
    
    
  }

  onClose(){
    this.event.publish("BackTo:NotRegistered",true);
  }

}
