import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-block',
  templateUrl: './block.page.html',
  styleUrls: ['./block.page.scss'],
})
export class BlockPage implements OnInit {

  constructor(    private modalCtrl:ModalController
    ) { }

  ngOnInit() {
  }

  async onClose(){
    await this.modalCtrl.dismiss();
  }

}
