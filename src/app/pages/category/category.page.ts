import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Categoria } from 'src/app/models/categoria';
import { Producto } from 'src/app/models/producto';
import { CartServiceService } from 'src/app/services/cart-service.service';
import { Storage } from '@ionic/storage';
import { Cart } from 'src/app/models/cart';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { NavParams, Events } from '@ionic/angular';
import { Establecimiento } from 'src/app/models/establecimiento';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  private user:User=new User;


  currentNumber = 0;

  category:Categoria;
  private establecimientos:Establecimiento[]=[];
  private products:Producto[]=[];

  private ProdCat:Producto[]=[];
  estabCat:Establecimiento[]=[];

  private searchBool:boolean=false;

  constructor(
    // public navParams: NavParams,
    private cartService:CartServiceService,
    private activatedRoute: ActivatedRoute,
    private storage:Storage,
    private router:Router,
    private events:Events
    // private activatedRoute: ActivatedRoute
  ) { 
    
    this.activatedRoute.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {

        if(this.router.getCurrentNavigation().extras.state.buscar){
          this.searchBool=true;
        }
        
        this.category = this.router.getCurrentNavigation().extras.state.category;
      }
    });
    this.storage.get("establecimientos").then((data:Establecimiento[])=>{
      this.establecimientos=data;  
      this.estabCat=this.productCats(this.category.id,this.establecimientos);
    
    });
    
    this.updateCartData();

    this.storage.get("cart").then((data:Cart)=>{
      this.currentNumber=data.quantity;
    });

    this.events.subscribe('cart:saved',(bool)=>{
      if(bool){
        this.storage.get('cart').then((data:Cart)=>{
          if(data)
          {
            this.currentNumber=data.quantity;
          }
          else{
            // this.currentNumber=data.quantity;
          }
        });
      }
    });
  }

  ionViewWillEnter(){
    
  }

  ngOnInit() {
    // this.category=this.navParams.get("category");
    // this.activatedRoute.params.subscribe((params) => {
    //   this.category=params.category;
    // });

    
  }


  productCats(catid:any,estabs:Establecimiento[]){
    let array:Establecimiento[]=[];
    for(var estab of estabs){
        
      if(estab["categoria_id"]===catid){
        array.push(estab);
      }
        
      
    }
    
    return array;
    

  }

  openProduct(estab:Establecimiento){
    // this.navCtrl.push(ProductPage,{producto:p});
    

    // this.navController.navigateForward('/category',{category:cat});
    if(this.searchBool){
      let navigationExtras: NavigationExtras = {
        state: {
          establecimiento: estab,
          buscar:true
        }
      };
      this.router.navigateByUrl('/tabs/buscar/see-more',navigationExtras);
    }
    else{
      let navigationExtras: NavigationExtras = {
        state: {
          establecimiento: estab
        }
      };
      this.router.navigateByUrl('/tabs/home/see-more',navigationExtras);
    }
    

  }

  onAdd(prod:Producto){
    
    // this.currentNumber=this.currentNumber+1;

    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        // alert(JSON.stringify(data))
        // this.machines.getMachinesByLimit(data.token,data.id,this.lat,this.lng);
        // this.rentService.getCurrent(data.id,data.token);
        
        // this.userLogged=true;
        this.user=data;
        // alert(JSON.stringify(this.user));
        this.cartService.addProductToCart(prod,1,this.user);
      }
      else{
        // this.userLogged=false;
      }
      
    });
    
  }

  updateCartData(){
    this.events.subscribe('cartUpdate:addedProduct',(val:boolean)=>{
      if(val){
        
        this.storage.get('cart').then((value:Cart) => {
          this.currentNumber=value.quantity;
        });
      }
      
      
    });
    this.events.subscribe('cartUpdate:deletedProduct',(val:boolean)=>{
      if(val){
        
        this.storage.get('cart').then((value:Cart) => {
          this.currentNumber=value.quantity;
        });
      }
      
      
    });

  }

  onOpenShoppingCart(){
    // const modal=this.modalCtrl.create(CartPage);
    // modal.present();
    this.router.navigateByUrl("/cart");
  }

}
