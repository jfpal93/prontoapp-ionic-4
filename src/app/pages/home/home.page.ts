import { Component, OnInit } from '@angular/core';
import { Events, LoadingController, ToastController, NavController, ModalController } from '@ionic/angular';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { User } from 'src/app/models/user';
import { Publicidad } from 'src/app/models/publicidad';
import { Categoria } from 'src/app/models/categoria';
import { Producto } from 'src/app/models/producto';
import { Favorito } from 'src/app/models/favorito';
import { Cart } from 'src/app/models/cart';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { CartServiceService } from 'src/app/services/cart-service.service';
import { forkJoin } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';
import { ImageAttribute } from 'ionic-image-loader'
import { Establecimiento } from 'src/app/models/establecimiento';
import { OrdenCompletaPage } from '../orden-completa/orden-completa.page';
import { PublicityPage } from '../publicity/publicity.page';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  imageAttributes: ImageAttribute[] = [];
  imageAttributesPUB: ImageAttribute[] = [];

  slideOptsCat = {
    initialSlide: 0,
    spaceBetween: 7,
    slidesPerView:2
  };

  slideOptsPub = {
    initialSlide: 0,
    spaceBetween: 7,
    slidesPerView:1
  };

  slideOptsFav = {
    initialSlide: 0,
    spaceBetween: 7,
    slidesPerView:1
  };

  private user:User=new User;
  currentNumber = 0;
  publicidades: Publicidad[]=[];
  
  publicidad1: Publicidad[]=[];
  private publicidad2: Publicidad[]=[];
  private publicidad3: Publicidad[]=[];
  private productos: Producto[]=[];
  categorias: Categoria[]=[];
  favoritos: Favorito[]=[];
  private establecimientos:Establecimiento[]=[];

  private cart:Cart=new Cart;

  private userLogged:boolean=false;

  

  constructor(
    private apiService:ApiServiceService,
    private storage:Storage,
    public event:Events,
    public loadingController:LoadingController,
    public locationAccuracy:LocationAccuracy,
    public toastCtrl:ToastController,
    private cartService:CartServiceService,
    private navController:NavController,
    private router:Router,
    private modalCtrl:ModalController

    // private imageAttributes: ImageAttribute

  ) { 
    this.imageAttributes.push({
      element: 'class',
      value: 'thumb'
    });
    this.imageAttributesPUB.push({
      element: 'class',
      value: 'thumb2'
    });
    // this.router.navigateByUrl("/smsconfirmation");

    this.storage.get("cart").then((data:Cart)=>{
      this.currentNumber=data.quantity;
    });
      
    this.event.subscribe('initapp',(bool)=>{
      if(bool){
        this.loadData();
      }
    });

    this.event.subscribe('cart:saved',(bool)=>{
      if(bool){
        this.storage.get('cart').then((data:Cart)=>{
          if(data)
          {
            this.currentNumber=data.quantity;
          }
          else{
            // this.currentNumber=data.quantity;
          }
        });
      }
    });

    

    this.event.subscribe('order:placed',(bool)=>{
      if(bool){
        this.loadData();
        
      }
      
    });

    // this.event.subscribe('user:created',(bool)=>{
    //   if(bool){
    //     this.storage.get('cart').then((data:Cart)=>{
    //       if(data)
    //       {
    //         this.currentNumber=data.quantity;
    //       }
    //       else{
    //         // this.currentNumber=data.quantity;
    //       }
    //     });
        
    //   }
      
    // });
    
    this.updateCartData();
  }

  ngOnInit() {
    // this.loadData();
  }

  ionViewWillEnter(){
    
  }

  async showToast(msj:string){
    const DeclinedToast =await  this.toastCtrl.create({
        message: msj,
        duration: 4000,
        position: 'bottom',
        buttons: [ {
            text: 'Cerrar',
            role: 'cancel'
          }
        ]
    });
    DeclinedToast.present();
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      return null;
    }
  }

  detectSpaces(word:string){
    let splt = word.split(" ");
    if(splt.length == 1){
      return true;
    }
    return false;
  }




  async getData() {
    const loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await loading.present();

    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.apiService.getData(data)
        .subscribe(res => {
          Promise.all([
            this.saveData("publicidads",res[0]),
            this.saveData("categorias",res[1]),
            this.saveData("favoritos",res[2]),
            this.saveData("productos",res[3]),
            this.saveData("establecimientos",res[4])
          ]).then(values => { 
            this.event.publish("savedData",true);
          });
          loading.dismiss();
        }, err => {
          this.showToast(err);
          loading.dismiss();        
        });
        
      }
      else{
        this.apiService.getData(data)
        .subscribe(res => {
          Promise.all([
            this.saveData("publicidads",res[0]),
            this.saveData("categorias",res[1]),
            this.saveData("favoritos",res[2]),
            this.saveData("productos",res[3]),
            this.saveData("establecimientos",res[4])
          ]).then(values => { 
            this.event.publish("savedData",true);
          });
          loading.dismiss();
        }, err => {
          this.showToast(err);
          loading.dismiss();
        });
      }
      
    });
  }

  saveData(key:string,dat){
    return new Promise((resolve, reject) => { 
      this.storage.set(key,dat).then(()=>{
        resolve("Collected!!");
      });
    });
  }

  // getCart(){
  //   this.storage.get('user').then((data:User)=>{
  //     if(data)
  //     {
  //       return new Promise((resolve, reject) => {
  //         resolve(this.cartService.getCart(data));
  //       });
  //     }
  //     else{
  //       return new Promise((resolve, reject) => { 
  //         resolve(this.cartService.createCart());
  //       });
  //     }
  //   });
  // }

  loadData(){
    this.publicidad1=[];
    // this.publicidad2=[];
    // this.publicidad3=[];
    this.productos=[];
    this.categorias=[];
    this.favoritos=[];


    Promise.all([
      this.storage.get("productos"),
      this.storage.get("favoritos"),
      this.storage.get("categorias"),
      this.storage.get("publicidads"),
      this.storage.get("cart"),
      this.storage.get("establecimientos")
    ]).then(values => { 
        this.categorias=values[2];
        this.favoritos=values[1];
        
        
        
        this.productos=values[0];
        this.currentNumber=values[4].quantity;
        for(var dat of values[3]){
          if(dat.orden ==="1"){
            this.publicidad1.push(dat);
          }
        }
        this.publicidades=values[3];
        this.establecimientos=values[5];
        
    });
  }

  returnProductsFavs(detail:any[]){
    let array:Producto[]=[];
    for(var it in detail){
      for(var prod in this.productos){
        if(detail[it].producto_id == this.productos[prod]["id"]){
          array.push(this.productos[prod]);
        }
      }
    }
    return array;
  }

  returnProductEstab(detail:Producto){
    // let array:Producto[]=[];
    for(var estab in this.establecimientos){
      
      if(detail.establecimiento_id == this.establecimientos[estab]['id']){
        return this.establecimientos[estab]['nombre'];
      }
    }
    
    
  }


  onAdd(prod:Producto){
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.cartService.addProductToCart(prod,1,data);
      }
      else{
      this.cartService.addProductToCart(prod,1,data);
      }
    });
    
  }

  async openPublicity(pub:Publicidad){
    let pubs:any[]=[];
    
    this.storage.get("productos").then(async (dat:Producto[])=>{
      if(dat){
        
        let count=0;
        for(let p of pub['detalle']){
          for(let d of dat){
            if(p.producto_id===d.id && d.disponible){
              count++;
              pubs.push(p);
            }
          }
        }
        pub['detalle']=pubs;

        if(count>0){
          const modal = await this.modalCtrl.create({
            component:PublicityPage,
            componentProps:{
              data:pub,
              prod:dat,
              cssClass: 'modal-transparency',
              backdropDismiss: true
            }
          });
          modal.present();
        }
        
      }
    });
    
    
  }

  onOpenShoppingCart(){
    // const modal=this.modalCtrl.create(CartPage);
    // modal.present();
    this.router.navigateByUrl("/cart");
  }


  openCategory(cat:Categoria){
    let navigationExtras: NavigationExtras = {
      state: {
        category: cat
      }
    };
    this.navController.navigateForward(['/tabs/home/category'], navigationExtras);

    // this.router.navigateByUrl('/tabs/home/category',navigationExtras);

  }

  openSeeMore(fav:Favorito){
    let navigationExtras: NavigationExtras = {
      state: {
        favorite: fav
      }
    };
    this.navController.setDirection('forward');
    this.navController.navigateForward(['/tabs/home/see-more'], navigationExtras);

    // this.router.navigate(['/tabs/home/see-more'],navigationExtras)
  }

  openProduct(p:Producto){
    let navigationExtras: NavigationExtras = {
      state: {
        product: p
      }
    };
    this.router.navigate(['/tabs/home/product'],navigationExtras)
  }

  updateCartData(){

    this.event.subscribe('cartUpdate:addedProduct',(bool:boolean)=>{
      if(bool){
        this.storage.get("cart").then((data:Cart)=>{
          this.currentNumber=data.quantity;
          
        });
      }
      
      
    });

    this.event.subscribe('cartUpdate:deletedProduct',(bool:boolean)=>{
      if(bool){
        this.storage.get("cart").then((data:Cart)=>{
          this.currentNumber=data.quantity;
          
        });
      }
      
      
    });

  }

  printPub(pub:Publicidad,i:number){
    let calc=(i/2)+2;

    if(Number(pub.orden)==calc){
      return true;
    }
    return false;

  }
}
