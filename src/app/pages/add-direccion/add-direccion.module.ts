import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddDireccionPage } from './add-direccion.page';
import { AgmCoreModule } from '@agm/core';
import { BlockPage } from '../block/block.page';

const routes: Routes = [
  {
    path: '',
    component: AddDireccionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey:
      'AIzaSyAt7Mk0F78UZ5Oj33N0IPpZE8LgauIugYM'
      
    })
  ],
  declarations: [AddDireccionPage,BlockPage],
  entryComponents:[BlockPage]
})
export class AddDireccionPageModule {}
