import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DireccionServiceService } from 'src/app/services/direccion-service.service';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NavController, Events, Platform, ToastController, ModalController } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { Direccion } from 'src/app/models/direccion';
import { Location } from '@angular/common';
import { BlockPage } from '../block/block.page';

@Component({
  selector: 'app-add-direccion',
  templateUrl: './add-direccion.page.html',
  styleUrls: ['./add-direccion.page.scss'],
})
export class AddDireccionPage implements OnInit {

  lat:number=0;
  latt:number=0;
  lng:number=0;

  dirID:Number;

  direccionData = { 
    nombre:'',
    direccion: '',
    lat:0.00,
    lng:0.00
  };

  iconMap={
    // url:"../../assets/imgs/pin2.png",
    scaledSize: {
      width: 30,
      height:60

    }
  }

  private geoFence:any[]=[];

  edit:boolean=false;

  private xCoord:number[]=[];
    private yCoord:number[]=[];

  constructor(
    public event:Events,
    private storage:Storage,
    private dirService:DireccionServiceService,
    private geolocation:Geolocation,
    private locationAccuracy:LocationAccuracy,
    private diagnostic:Diagnostic,
    public navCtrl: NavController,
    private route: ActivatedRoute, 
    private router: Router,
    private location: Location,
    private platform:Platform,
    private toastCtrl:ToastController,
    private modalCtrl:ModalController
  ) {
    
    this.event.subscribe('user:direccion-created',(bool)=>{
      if(bool){
        this.closeModal();

      }
    });

    this.event.subscribe('user:direccion-edited',(bool)=>{
      if(bool){
        this.closeModal();

      }
    });

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        if (this.router.getCurrentNavigation().extras.state.edit){
          this.edit=true;
          let dir:Direccion=this.router.getCurrentNavigation().extras.state.direccion;
          // this.data = this.router.getCurrentNavigation().extras.state.user;
          this.direccionData.nombre=dir.nombre;
          this.direccionData.direccion=dir.direccion;
          this.direccionData.lat=dir.lat;
          this.direccionData.lng=dir.lng;
          this.dirID=dir.id;

          this.lat=dir.lat;
          this.lng=dir.lng;
          this.latt=dir.lat+Number((0.00250000000));
        }

      }
      else{
        
        this.getLocation();
      }
    });

    
   }

  ngOnInit() {
    
  }

  loadGeoFence(){
    this.geoFence=[];
    

    Promise.all([
      this.storage.get("coordenadas")
    ]).then(values => { 
      for(let c of values[0])
      {
        let arr={ "lat": parseFloat(c.lat), "lng": parseFloat(c.lng) }

        this.geoFence.push(arr);
      }
      for (var key in this.geoFence) {
        this.yCoord.push(this.geoFence[key].lat);
        this.xCoord.push(this.geoFence[key].lng);
      }
      
    });
  }

  pointInPolygon(location:any):boolean {

    let   i, j=this.xCoord.length-1 ;
    let  oddNodes=false;
    for (i=0; i<this.xCoord.length; i++) {
      if ((this.yCoord[i]< location.lat && this.yCoord[j]>=location.lat
      ||   this.yCoord[j]< location.lat && this.yCoord[i]>=location.lat)
      &&  (this.xCoord[i]<=location.lng || this.xCoord[j]<=location.lng)) {
        if (this.xCoord[i]+(location.lat-this.yCoord[i])/(this.yCoord[j]-this.yCoord[i])*(this.xCoord[j]-this.xCoord[i])<location.lng) {
          oddNodes=!oddNodes; }}
      j=i; }
  
    return oddNodes; 
  }

  ionViewWillEnter(){
    // if (this.platform.is('android')) {
    // this.getLocation();
    
    // if (this.platform.is('android')) {
    //   this.androidLocation();
    // }
    // if (this.platform.is('ios')) {
      // this.iosLocation();
    // }
    // this.requestLocationAccuracy();
    // this.requestLocationAuthorization();
    // this.diagnostic.requestLocationAuthorization()
    // .then((value)=>{
    //   alert("request status"+value);
    //   // this.handleLocationAuthorizationStatus(value);
    // }).catch((error)=>{
    //   // this.onError(error);
    // });
  }

  iosLocation(){
    this.diagnostic.isLocationEnabled().then((isAvailable)=>{
        
      if(!isAvailable){
        this.diagnostic.requestLocationAuthorization().then((value)=>{
          alert(value);
        });
        // this.locationAccuracy.canRequest().then((canRequest:boolean)=>{
        //   alert(canRequest);
        //   if(canRequest){
            
        //   }
        //   else{
        //     // this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        //     //   () => {
        //     //     this.getLocation();
        //     //   },
        //     //   error => {
                
        //     //   }
        //     // );
        //   }
        // }).catch((e)=>{
        //   alert(e);
        // });
      }

    }).catch( (e) => {
      // alert(e);
    });
  }

  onError(error) {
      alert("The following error occurred: " + JSON.stringify(error));
  }

  

  // handleLocationAuthorizationStatus2(status) {
  //   // alert(status)
  //   switch (status) {
  //       case "GRANTED":
  //           if(this.platform.is('ios')){
  //               this.onError("Location services is already switched ON");
  //           }else{
  //               this._makeRequest();
  //           }
  //           break;
  //       case "DENIED":
  //           this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
  //           this.closeModal();
  //           break;
  //       case "DENIED_ONCE":
  //           this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
  //           this.closeModal();
  //           break;
            
  //       case "DENIED_ALWAYS":
  //           // Android only
  //           this.showToast("Necesitamos tu ubicación para poder enviar tu pedido...");
  //           this.closeModal();
  //           break;
  //       case "GRANTED_WHEN_IN_USE":
  //           // iOS only
  //           this.onError("Location services is already switched ON");
  //           break;
  //   }
  // }

  async showToast(msj:string){
          
    const DeclinedToast =await  this.toastCtrl.create({
        message: msj,
        duration: 4000,
        position: 'bottom',
        buttons: [ {
            text: 'Cerrar',
            role: 'cancel'
          }
        ]
    });
    DeclinedToast.present();
  }

  

  
  

  

  androidLocation(){
    this.diagnostic.isLocationEnabled().then((isAvailable)=>{
        
      if(!isAvailable){
        // this.checkGPSPermission();
        // this.locationAccuracy.canRequest().then((canRequest:boolean)=>{
        //   alert(canRequest)
        //   if(canRequest){
        //     alert("carequest");
            // this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            //   () => {
            //     this.getLocation();
            //   },
            //   error => {
                
            //   }
            // );
            
        //   }
        //   else{
        //     alert(canRequest)
        //   }
        // }).catch((e)=>{
        //   // alert(e)
        // });
      }

    }).catch( (e) => {
      // alert(e);
    });
  }

  getLocation(){
    this.geolocation.getCurrentPosition().then((resp)=>{
      this.geoFence=[];
    

      Promise.all([
        this.storage.get("coordenadas")
      ]).then(async values => { 
        for(let c of values[0])
        {
          let arr={ "lat": parseFloat(c.lat), "lng": parseFloat(c.lng) }

          this.geoFence.push(arr);
        }

        for (var key in this.geoFence) {
          this.yCoord.push(this.geoFence[key].lat);
          this.xCoord.push(this.geoFence[key].lng);
          
      
        }

        this.lat=resp.coords.latitude;
        this.lng=resp.coords.longitude;
        this.latt=resp.coords.latitude+Number((0.00250000000));

        this.direccionData.lat=resp.coords.latitude;
        this.direccionData.lng=resp.coords.longitude;

        if(!this.pointInPolygon({lat:this.lat,lng:this.lng})){
          const modal = await this.modalCtrl.create({
            component:BlockPage,
            componentProps:{
              cssClass: 'modal-transparency'
            }
          });
          modal.present();
          modal.onDidDismiss().then(data=>{
            this.closeModal();
          });
        }
        
      });
      // this.loadGeoFence();
      
      
    }).catch((e)=>{
      alert("geolocation error "+e)
    });
  }

  closeModal(){
    // this.viewCtrol.dismiss();
    this.navCtrl.pop();
  }

  send(){
    if(this.edit){
      this.storage.get('user').then((data:User)=>{
        if(data)
        {
          this.dirService.editDireccion(this.direccionData,data,this.dirID);
        }
        
      });
    }
    else{
      this.storage.get('user').then((data:User)=>{
        if(data)
        {
          this.dirService.addDireccion(this.direccionData,data);
        }
        
      });
    }
    

  }

  markerDragEnd(evnt:any){
    // alert(JSON.stringify(evnt));
    

    this.lat=evnt.coords.lat;
    this.lng=evnt.coords.lng;
    this.latt=evnt.coords.lat+Number((0.00250000000));

    this.direccionData.lat=evnt.coords.lat;
    this.direccionData.lng=evnt.coords.lng;

  }

  click(evnt:any){
    this.lat=evnt.coords.lat;
    this.lng=evnt.coords.lng;
    this.latt=evnt.coords.lat+Number((0.00250000000));

    this.direccionData.lat=evnt.coords.lat;
    this.direccionData.lng=evnt.coords.lng;
  }


  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
      } else {
        //Show 'GPS Permission Request' dialogue
        // this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
        //   .then(
        //     () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
          //   },
          //   error => {
          //     //Show alert if user click on 'No Thanks'
          //     alert('requestPermission Error requesting location permissions ' + JSON.stringify(error))
          //   }
          // );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocation();
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

}
