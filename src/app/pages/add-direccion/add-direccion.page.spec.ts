import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDireccionPage } from './add-direccion.page';

describe('AddDireccionPage', () => {
  let component: AddDireccionPage;
  let fixture: ComponentFixture<AddDireccionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDireccionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDireccionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
