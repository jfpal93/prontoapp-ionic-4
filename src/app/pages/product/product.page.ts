import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Categoria } from 'src/app/models/categoria';
import { Producto } from 'src/app/models/producto';
import { Cart } from 'src/app/models/cart';
import { Events } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { CartServiceService } from 'src/app/services/cart-service.service';
import { Establecimiento } from 'src/app/models/establecimiento';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  private user:User=new User;


  product:Producto;
  private categories:Categoria[]=[];

  currentNumber = 0;

  currentProdNumber=1;

  private searchBool:boolean=false;

  private establecimientos:Establecimiento[]=[];

  constructor(
    private events:Events,
    private storage:Storage,
    private activatedRoute: ActivatedRoute,
    private router:Router,
    private cartService:CartServiceService

  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {

        if(this.router.getCurrentNavigation().extras.state.buscar){
          this.searchBool=true;
        }
        
        this.product = this.router.getCurrentNavigation().extras.state.product;
      }
    });
      
      this.storage.get("establecimientos").then((data:Establecimiento[])=>{
        this.establecimientos=data; 
      });

      this.storage.get("categorias").then((data:Categoria[])=>{
        this.categories=data; 
      });

      this.storage.get("cart").then((data:Cart)=>{
        this.currentNumber=data.quantity;
      });

      this.updateCartData();
   }

  ngOnInit() {
  }

  returnCategoryByID(){
    
    for(var estab of this.establecimientos){
      if(this.product["establecimiento_id"] == estab["id"]){
        for(var cat of this.categories){
          if(estab["categoria_id"] == cat["id"]){
            return cat["nombre"];
          }
        }
        
      }
    }
  }

  increment () {
    if(this.currentProdNumber>=1){
      this.currentProdNumber++;
    }
    
  }
  
  decrement () {
    if(this.currentProdNumber>1){
      this.currentProdNumber--;
    } 
  }


  onAdd(){
    
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.cartService.addProductToCart(this.product,this.currentProdNumber,data);
      }
      else{
      this.cartService.addProductToCart(this.product,this.currentProdNumber,data);
      }
    });
    
  }

  onDelete(){

    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        // alert(JSON.stringify(data))
        // this.machines.getMachinesByLimit(data.token,data.id,this.lat,this.lng);
        // this.rentService.getCurrent(data.id,data.token);
        
        // this.userLogged=true;
        this.user=data;
        this.cartService.deleteProductToCart(this.product,this.user);
      }
      else{
        // this.userLogged=false;
      }
      
    });
    
    // this.decrement();
  }
  updateCartData(){

      this.events.subscribe('cartUpdate:addedProduct',(bool:boolean)=>{
        if(bool){
          this.storage.get('cart').then((val:Cart) => {
            this.currentNumber=val.quantity;
          });
        }
        
        
      });
      this.events.subscribe('cartUpdate:deletedProduct',(bool:boolean)=>{
        if(bool){
          this.storage.get('cart').then((val:Cart) => {
            this.currentNumber=val.quantity;
          });
        }
        
        
      });

    }

    onOpenShoppingCart(){
      // const modal=this.modalCtrl.create(CartPage);
      // modal.present();
      this.router.navigateByUrl("/cart");
    }

}
