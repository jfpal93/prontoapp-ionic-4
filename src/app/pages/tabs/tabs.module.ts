import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { OrderConfirmationPage } from '../order-confirmation/order-confirmation.page';
import { OrderArrivedPage } from '../order-arrived/order-arrived.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule
  ],
  declarations: [TabsPage,OrderConfirmationPage,OrderArrivedPage],
  entryComponents:[OrderConfirmationPage,OrderArrivedPage]
})
export class TabsPageModule {}
