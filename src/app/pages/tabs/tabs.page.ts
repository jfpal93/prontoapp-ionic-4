import { Component, ViewChild } from '@angular/core';
import { IonTabs, NavController, Events, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { User } from 'src/app/models/user';
import { Event } from '@angular/router';
import { CartServiceService } from 'src/app/services/cart-service.service';
import { OrderConfirmationPage } from '../order-confirmation/order-confirmation.page';
import { OrderArrivedPage } from '../order-arrived/order-arrived.page';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})

export class TabsPage {

  private tabs:any;

  private selected:string;

  constructor(
    private storage:Storage,
    private navCtrl:NavController,
    private event:Events,
    private cartService:CartServiceService,
    private modalCtrl:ModalController
  ) {
    // this.showConfirmationPage();
    // this.showArrivedPage();
    this.event.subscribe('BackTo:NotLoggedIn',(bool:boolean)=>{
      if(bool){
        this.tabs.select('home');
      }
    });

    this.event.subscribe('openSignUp',(bool:boolean)=>{
      if(bool){
        this.navCtrl.navigateForward("register");
      }
    });

    this.event.subscribe('BackTo:NotRegistered',(bool:boolean)=>{
      if(bool){
        this.tabs.select('home');

      }
    });

    this.event.subscribe('user:created',(bool)=>{
      if(bool){
        this.tabs.select(this.selected);
        // this.storage.get('user').then((user:User)=>{
        //   this.cartService.getCart(user);
        // });
      }
      
    });

    this.event.subscribe('user:entered',(bool)=>{
      if(bool){
        this.tabs.select(this.selected);
        
      }
      
    });

    // this.event.subscribe('cart:closed',(bool)=>{
    //   if(bool){
    //     this.tabs.select(this.selected);
    //   }
      
    // });

    this.event.subscribe('order:placed',(bool)=>{
      if(bool){
        this.tabs.select('pedidos');
      }
      
    });

    this.event.subscribe('Order:Confirmation',(bool)=>{
      if(bool){
        
        this.showConfirmationPage();
        
      }
      
    });

    this.event.subscribe('Order:arrived',(bool)=>{
      if(bool){
        this.showArrivedPage();
        
      }
      
    });

    this.event.subscribe('user:loggedout',(bool)=>{
      if(bool){
        this.tabs.select('home');
      }
      
    });
    
  }

  ionChange(myTabs) {
    this.tabs=myTabs;
    this.selected = this.tabs.getSelected();
    
    if(this.selected==='pedidos' || this.selected==='perfil'){
      this.storage.get("user").then((data:User)=>{
        if(data){
          this.tabs.select(this.selected)
        }
        else{
          this.navCtrl.navigateForward('/login');
        }
  
      });
    }
  }

  showConfirmationPage(){
    this.storage.get('orderConfirmationData').then(async (dat:any)=>{
      if(dat){
        const modal = await this.modalCtrl.create({
          component:OrderConfirmationPage,
          componentProps:{
            data:dat,
            cssClass: 'modal-transparency',
            backdropDismiss: true
          }
        });
        modal.present();
        // const { data } = await modal.onWillDismiss();
        // if(data.dismissed){
        //   this.event.unsubscribe('Order:Confirmation');
        // }
      }
    });
    
  }

  showArrivedPage(){
    this.storage.get('orderData').then(async (data:any)=>{
      if(data){
        const modal = await this.modalCtrl.create({
          component:OrderArrivedPage,
          componentProps:{
            data:data,
            cssClass: 'modal-transparency'
          }
        });
        modal.present();
      }
    })
    
  }

}
