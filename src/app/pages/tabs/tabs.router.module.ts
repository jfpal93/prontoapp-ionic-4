import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          },
          {
            path: 'category',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../category/category.module').then(m => m.CategoryPageModule)
              }
            ]
          },
          {
            path: 'see-more',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../see-more/see-more.module').then(m => m.SeeMorePageModule)
              }
            ]
          },
          {
            path: 'product',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../product/product.module').then(m => m.ProductPageModule)
              }
            ]
          }
        ]
      },
      {
        path: 'buscar',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../buscar/buscar.module').then(m => m.BuscarPageModule)
          },
          {
            path: 'category',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../category/category.module').then(m => m.CategoryPageModule)
              }
            ]
          },
          {
            path: 'see-more',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../see-more/see-more.module').then(m => m.SeeMorePageModule)
              }
            ]
          },
          {
            path: 'product',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../product/product.module').then(m => m.ProductPageModule)
              }
            ]
          }
        ]
      },
      {
        path: 'pedidos',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pedidos/pedidos.module').then(m => m.PedidosPageModule)
          }
        ]
      },
      {
        path: 'perfil',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../perfil/perfil.module').then(m => m.PerfilPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
