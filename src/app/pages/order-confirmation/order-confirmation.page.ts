import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, ModalController, NavParams, Platform } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { PedidosService } from 'src/app/services/pedidos.service';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.page.html',
  styleUrls: ['./order-confirmation.page.scss'],
})
export class OrderConfirmationPage implements OnInit {

  private data:any;
  codigo:string;
  products:any[]=[];

  subtotal:number;
  costo_delivery:number;
  total:number;
  tiempoTotal:number;

  shownGroup=null;
  private ordenId:number;

  private user:User;

  private backButtonSubscription;

  constructor(
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private events:Events,
    private storage:Storage,
    private pedidoService:PedidosService,
    private platform:Platform
  ) {
    this.data=JSON.parse(this.navParams.get("data"));
    
    this.codigo=this.data.orden_codigo;
    this.products=this.data.products;
    this.subtotal=this.data.subtotal;
    this.costo_delivery=this.data.costo_delivery;
    this.total=this.data.total;
    this.tiempoTotal=this.data.tiempo;
    this.ordenId=this.data.orden_id;

    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.user=data;

      }      
    });

    this.platform.resume.subscribe(async () => {
      await this.modalCtrl.dismiss();
     }); 

    
   }

   ionViewDidEnter(){
    this.backButtonSubscription=this.platform.backButton.subscribeWithPriority(9999, () => {
      // Run your logic to decide either to close or not
    });
    
   }

   ionViewDidLeave() {
      if (this.backButtonSubscription && !this.backButtonSubscription.closed) {
        this.backButtonSubscription.unsubscribe();
      }
    }

   ionViewWillEnter(){
    
   }

  ngOnInit() {
  }

  async deliveryAccepted(){
    this.pedidoService.acceptDelivery(this.user,this.ordenId);
    await this.modalCtrl.dismiss();

  }

  async cancelDelivery(){
    this.pedidoService.cancelDelivery(this.user,this.ordenId);
    await this.modalCtrl.dismiss();
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  }

  isGroupShown(group) {
      return this.shownGroup === group;
  }

  async onClose(){
    await this.modalCtrl.dismiss();
    // this.modalCtrl.dismiss({
    //   'dismissed': true
    // });
  }

}
