import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Producto } from 'src/app/models/producto';
import { Favorito } from 'src/app/models/favorito';
import { ActivatedRoute, Route, Router, NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { CartServiceService } from 'src/app/services/cart-service.service';
import { Cart } from 'src/app/models/cart';
import { Events } from '@ionic/angular';
import { Establecimiento } from 'src/app/models/establecimiento';

@Component({
  selector: 'app-see-more',
  templateUrl: './see-more.page.html',
  styleUrls: ['./see-more.page.scss'],
})
export class SeeMorePage implements OnInit {

  private user:User=new User;


  currentNumber = 0;

  ProdFav:Producto[]=[];
  private products:Producto[]=[];
  favorito:Favorito;

  private estabProds:Producto[]=[];

  estableciminto:Establecimiento;

  private searchBool:boolean=false;

  constructor(
    private activatedRoute:ActivatedRoute,
    private router:Router,
    private storage:Storage,
    private cartservice:CartServiceService,
    private events:Events
  ) { 
    this.activatedRoute.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {

        if(this.router.getCurrentNavigation().extras.state.buscar){
          this.searchBool=true;
        }
        
        if(this.router.getCurrentNavigation().extras.state.favorite){
          this.favorito = this.router.getCurrentNavigation().extras.state.favorite;
          this.storage.get("productos").then((data:Producto[])=>{
            this.products=data;  
            this.ProdFav=this.productFavs(this.products);
          });
          
        }
        if(this.router.getCurrentNavigation().extras.state.establecimiento){
          this.estableciminto = this.router.getCurrentNavigation().extras.state.establecimiento;
          this.storage.get("productos").then((data:Producto[])=>{
            this.products=data;  
            this.ProdFav=this.productFavs(this.products);            
            
          });
          
        }
      }
    });

    
    

    this.updateCartData();

    this.storage.get("cart").then((data:Cart)=>{
      this.currentNumber=data.quantity;
    });

    this.events.subscribe('cart:saved',(bool)=>{
      if(bool){
        this.storage.get('cart').then((data:Cart)=>{
          if(data)
          {
            this.currentNumber=data.quantity;
          }
        });
      }
    });

  }

  ngOnInit() {
  }

  productFavs(prods:Producto[]){
    let array:Producto[]=[];

    if(this.favorito){
      for(var prodFav of this.favorito.detalle){

        for(var prod of prods){
          if(prod["id"]===prodFav["producto_id"]){
            array.push(prod);
          }
        }
        
      }
      return array;
    }
    if(this.estableciminto){
      // for(var prodFav of this.favorito.detalle){

        for(var prod of prods){
          if(prod.establecimiento_id===this.estableciminto.id){
            array.push(prod);
          }
        }
        
        
      // }
      return array;
    }
    
  }

  openProduct(p:Producto){
    // this.navCtrl.push(ProductPage,{producto:p});
    if(this.searchBool){
      let navigationExtras: NavigationExtras = {
        state: {
          product: p,
          buscar:true
        }
      };
      // this.navController.navigateForward('/product',{producto:p});
      this.router.navigate(['/tabs/buscar/product'],navigationExtras)
    }
    else{
      let navigationExtras: NavigationExtras = {
        state: {
          product: p,
          buscar:true
        }
      };
      // this.navController.navigateForward('/product',{producto:p});
      this.router.navigate(['/tabs/home/product'],navigationExtras)
    }

  }

  onAdd(prod:Producto){
    

    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.cartservice.addProductToCart(prod,1,data);
      }
      else{
      this.cartservice.addProductToCart(prod,1,data);
      }
    });
    
  }

  updateCartData(){

    this.events.subscribe('cartUpdate:addedProduct',(val:boolean)=>{
      if(val){
        
        this.storage.get('cart').then((value:Cart) => {
          this.currentNumber=value.quantity;
        });
      }
      
      
    });
    this.events.subscribe('cartUpdate:deletedProduct',(val:boolean)=>{
      if(val){
        
        this.storage.get('cart').then((value:Cart) => {
          this.currentNumber=value.quantity;
        });
      }
      
      
    });

  }

  onOpenShoppingCart(){
    this.router.navigateByUrl("/cart");
  }

}
