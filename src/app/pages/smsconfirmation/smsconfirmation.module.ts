import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SmsconfirmationPage } from './smsconfirmation.page';

const routes: Routes = [
  {
    path: '',
    component: SmsconfirmationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SmsconfirmationPage]
})
export class SmsconfirmationPageModule {}
