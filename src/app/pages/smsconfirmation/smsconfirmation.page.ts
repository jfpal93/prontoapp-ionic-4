import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SmsService } from 'src/app/services/sms.service';
import { LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-smsconfirmation',
  templateUrl: './smsconfirmation.page.html',
  styleUrls: ['./smsconfirmation.page.scss'],
})
export class SmsconfirmationPage implements OnInit {
  credentials={
    name:"",
    email: "",
    phone:"",
    lastname:"",
    password:'' 
  };
  private fireID:any;

  otp:"";

  // otp:number;

  constructor(
    private loadingCtrl:LoadingController,
    private smsService:SmsService,
    private router:Router,
    private auth:AuthService,
    private route: ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.credentials = this.router.getCurrentNavigation().extras.state.credentials;
        this.fireID = this.router.getCurrentNavigation().extras.state.fireID;
      }
    });
  }

  ngOnInit() {
    
  }

  async sendOTP(){
    const loading=await this.loadingCtrl.create({
      message:'Enviando código...'
    });
    await loading.present();
    this.smsService.sendOTP(this.fireID,this.otp.toString(),this.credentials).then(async (value)=>{
      await loading.dismiss();
      this.auth.signup(this.credentials);
      
    }).catch(async (error)=>{
      await loading.dismiss();
      alert(JSON.stringify(error))
    });;

  }

  async sendsms(){
    const loading=await this.loadingCtrl.create({
      message:'Enviando mensaje...'
    });
    await loading.present();
    this.smsService.sendSMS(this.credentials.phone.substr(1)).then(async ()=>{
      await loading.dismiss();
    });
  }

}
