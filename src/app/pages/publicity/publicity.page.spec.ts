import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicityPage } from './publicity.page';

describe('PublicityPage', () => {
  let component: PublicityPage;
  let fixture: ComponentFixture<PublicityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicityPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
