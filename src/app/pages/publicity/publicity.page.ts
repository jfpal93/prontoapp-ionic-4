import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Publicidad } from 'src/app/models/publicidad';
import { Producto } from 'src/app/models/producto';
import { Categoria } from 'src/app/models/categoria';
import { ModalController, NavParams, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { CartServiceService } from 'src/app/services/cart-service.service';

@Component({
  selector: 'app-publicity',
  templateUrl: './publicity.page.html',
  styleUrls: ['./publicity.page.scss'],
})
export class PublicityPage implements OnInit {
  private data:any;

  nombre;
  productos:Producto[]=[];
  detalle:any[];

  constructor(
    private modalCtrl:ModalController,
    public navParams: NavParams,
    private storage:Storage,
    private cartService:CartServiceService,
    private platform:Platform

    ) { 
      this.platform.resume.subscribe(async () => {
        await this.modalCtrl.dismiss();
      }); 
      
    }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.data=this.navParams.get("data");
    this.productos=this.navParams.get("prod");
    this.nombre=this.data.nombre;
    this.detalle=this.data.detalle;
  }

  async onClose(){
    await this.modalCtrl.dismiss();
  }

  returnProduct(prodId){
    for(let p of this.productos){
      if(p.id===prodId){
        return p;
      }
    }
  }

  returnImg(prod:Producto){
    return prod.linkImagen;
  }

  onAdd(prod:Producto){
    this.storage.get('user').then((data:User)=>{
      if(data)
      {
        this.cartService.addProductToCart(prod,1,data);
      }
      else{
      this.cartService.addProductToCart(prod,1,data);
      }
    });
  }

}
