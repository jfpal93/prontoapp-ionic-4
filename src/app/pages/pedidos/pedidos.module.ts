import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PedidosPage } from './pedidos.page';
import { AccordionGroupComponent } from 'src/app/components/accordion/accordion-group.component';
import { AccordionComponent } from 'src/app/components/accordion/accordion.component';
import { AccordionListComponent } from 'src/app/components/accordion list/accordionlist.component';
import { AccordionListGroupComponent } from 'src/app/components/accordion list/accordionlist-group.component';

const routes: Routes = [
  {
    path: '',
    component: PedidosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PedidosPage,AccordionGroupComponent , 
    AccordionComponent,AccordionListComponent, AccordionListGroupComponent]
})
export class PedidosPageModule {}
