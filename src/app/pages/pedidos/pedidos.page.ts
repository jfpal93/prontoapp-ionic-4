import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from 'src/app/models/user';
import { NavController, Events, IonSlides, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { PedidosService } from 'src/app/services/pedidos.service';
import { Orden } from 'src/app/models/orden';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  pedidos:Orden[];
  segment = 0;
  @ViewChild('slides', { static: true }) slider: IonSlides;

  constructor(
    private storage:Storage,
    private navCtrl:NavController,
    private events:Events,
    private router:Router,
    private location: Location,
    private pedidosService:PedidosService,
    private loadingCtrl:LoadingController
  ) { 
    this.events.subscribe('order:placed',(bool)=>{
      if(bool){
        this.loadPedidos();
      }
      
    });

    this.events.subscribe('Order:Confirmation',(bool)=>{
      if(bool){
        
        this.loadPedidos();
        
      }
      
    });

    this.events.subscribe('Order:arrived',(bool)=>{
      if(bool){
        this.loadPedidos();
        
      }
      
    });
    this.events.subscribe('user:entered',(bool)=>{
      if(bool){
        this.loadPedidos();
      }
      
    });

    this.events.subscribe('user:loggedout',(bool)=>{
      if(bool){
        this.pedidos=[];
      }
      
    });
    this.getPedidos();
    this.pedidoCanceladoEvent();
  }

  ngOnInit() {
    
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  ionViewWillEnter(){
    this.loadPedidos();
  }

  async loadPedidos(){
    const loading=await this.loadingCtrl.create({
      message:'Obteniendo pedidos...'
    });
    await loading.present();
    this.storage.get('user').then(async(data:User)=>{
      if(data)
      {
        this.pedidosService.getPedidos(data);
        await loading.dismiss();
      }  
      else{
        await loading.dismiss();
      }    
    });
  }

   getPedidos(){
    
    this.events.subscribe('pedidos:saved',(bool)=>{
      if(bool){
        this.storage.get('pedidos').then( (data:Orden[])=>{
          if(data)
          {
            
            this.pedidos=data;
          }
          
        });

      }
      
    });
  }

  pedidoCanceladoEvent(){
    this.events.subscribe('orden:canceled',(bool)=>{
      if(bool){
        this.loadPedidos();
      }
      
    });
  }

  cancelOrder(orderId:any){

    this.storage.get('user').then((data:User)=>{
      if(data){
        this.pedidosService.cancelOrder(data,orderId);
      }
      
    });

  }

  deliveryAccepted(orderId:any){
    this.storage.get('user').then((data:User)=>{
      if(data){
        this.pedidosService.acceptDelivery(data,orderId);

      }
      
    });
  }

}
