import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { 
    path: 'add-direccion', 
    loadChildren: () => import('./pages/add-direccion/add-direccion.module').then(m => m.AddDireccionPageModule)
  },
  // { 
  //   path: 'buscar', 
  //   loadChildren: () => import('./pages/buscar/buscar.module').then(m => m.BuscarPageModule)
  // },
  { 
    path: 'cart', 
    loadChildren: () => import('./pages/cart/cart.module').then(m => m.CartPageModule)
  },
  // { 
  //   path: 'category', 
  //   loadChildren: () => import('./pages/category/category.module').then(m => m.CategoryPageModule)
  // },
  { 
    path: 'checkout', 
    loadChildren: () => import('./pages/checkout/checkout.module').then(m => m.CheckoutPageModule)
  },
  // { 
  //   path: 'home', 
  //   loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  // },
  { 
    path: 'login', 
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  // { 
  //   path: 'orden-completa', 
  //   loadChildren: () => import('./pages/orden-completa/orden-completa.module').then(m => m.OrdenCompletaPageModule)
  // },
  // { 
  //   path: 'pedidos', 
  //   loadChildren: () => import('./pages/pedidos/pedidos.module').then(m => m.PedidosPageModule)
  // },
  // { 
  //   path: 'perfil', 
  //   loadChildren: () => import('./pages/perfil/perfil.module').then(m => m.PerfilPageModule)
  // },
  // { 
  //   path: 'product', 
  //   loadChildren: () => import('./pages/product/product.module').then(m => m.ProductPageModule)
  // },
  { 
    path: 'publicity', 
    loadChildren: () => import('./pages/publicity/publicity.module').then(m => m.PublicityPageModule)
  },
  { 
    path: 'register', 
    loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule)
  },
  { path: 'add-direction', loadChildren: './add-direction/add-direction.module#AddDirectionPageModule' },
  { path: 'smsconfirmation', 
    loadChildren: () => import('./pages/smsconfirmation/smsconfirmation.module').then(m => m.SmsconfirmationPageModule)
  },
  // {
  //   path: 'block',
  //   loadChildren: () => import('./pages/block/block.module').then( m => m.BlockPageModule)
  // },

  // { path: 'order-arrived', loadChildren: './order-arrived/order-arrived.module#OrderArrivedPageModule' },

  // { 
  //   path: 'order-confirmation', 
  //   loadChildren: () => import('./pages/order-confirmation/order-confirmation.module').then(m => m.OrderConfirmationPageModule)
  // },

  // { 
  //   path: 'see-more', 
  //   loadChildren: () => import('./pages/see-more/see-more.module').then(m => m.SeeMorePageModule)
  // }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
