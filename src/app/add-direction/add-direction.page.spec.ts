import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDirectionPage } from './add-direction.page';

describe('AddDirectionPage', () => {
  let component: AddDirectionPage;
  let fixture: ComponentFixture<AddDirectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDirectionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDirectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
