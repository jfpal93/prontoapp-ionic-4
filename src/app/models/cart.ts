import { ProductsCarted } from "./productsCarted";

export class Cart {
    $key: string;
    id:number;
    state:string;
    quantity:number;
    subtotal:number;
    total:number;
    products:ProductsCarted[];
}
