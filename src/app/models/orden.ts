import { Pedido } from './pedido';
import { Direccion } from './direccion';

export class Orden {
    $key: string;
    id:number;
    state:string;
    quantity:number;
    subtotal:number;
    total:number;
    products:Pedido[];
    codigo:string;
    cant:number;
    costo_delivery:number;
    tiempo:number;
    tiempoDel:number;
    tiempoEstabs:number;
    nombreCliente:string;
    direccion:Direccion;
    pedidos:Pedido[];
}
