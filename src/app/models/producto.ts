export class Producto {
    $key: string;
    id:number;
    linkImagen: string;
    nombre: string;
    precio:number;
    disponible:boolean;
    descripcion:string;
    establecimiento_id:number;
    nombreEstablecimiento:string;
    count:number;
}
