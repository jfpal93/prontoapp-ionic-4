export class Direccion {
    $key: string;
    id:number;
    nombre: string;
    user_id:number;
    direccion:string;
    lat:number;
    lng:number;
    selected:boolean;
}
