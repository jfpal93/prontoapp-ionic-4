export class Categoria {
    $key: string;
    id:number;
    linkImagen: string;
    linkIcono: string;
    nombre: string;
}
