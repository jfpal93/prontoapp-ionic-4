export class User {
    $key: string;
    id:number;
    name: string;
    lastname: string;
    email:string;
    access_token:string;
    phone:string;
}
