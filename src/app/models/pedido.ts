import { Producto } from "./producto";

export class Pedido {
    product:Producto;
    count:number;
    tiempo:number;
    subtotal:number;
    total:number;
    establecimiento:string;
    estabId:number;
    id:number;
    products:Producto[];
}
